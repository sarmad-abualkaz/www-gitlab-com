---
layout: job_family_page
title: "People Operations"
---

## People Operations Specialist

## Levels

### People Operations Specialist (Intermediate)

The People Operations Specialist (Intermediate) reports to the Manager, People Operations.

#### People Operations Specialist (Intermediate) Job Grade

The People Operations Specialist (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### People Operations Specialist (Intermediate) Responsibilities

- Provide backup / guidance to the People Experience team in responding to emails in the people operations email and Slack channel to ensure extraordinary customer service to employees and leaders at GitLab.
- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Program management including rollout, communication, reporting and metrics of Organizational health, Engagement Survey, DIB related surveys, 360 feedback cycle and Performance/ Potential Matrix Assessment. 
- Implementation and rollout of a stay interview process (analysis and actions). 
- Intake of questions, complaints and concerns.
- Escalating concerns or problems to People Business Partners as needed.
- Oversee probation period strategy, collaborating with the legal team to ensure compliance and iteration, globally. Document all information and updates in the handbook.
- Identify automation opportunities for all People Operations tasks and iterate on manual administrative processes.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel
- Engagement survey administration, implementation and reporting.
- Provide support and cover for Associates including completion of Onboarding issues and Offboarding issues.
- DRI for reporting potential inaccurate LinkedIn profiles.
- Providing support to the Senior Specialist regarding contract changes and administration.
- Engage with co-employer’s and PEO's on a regular basis, especially during onboarding within those regions.
- Identify gaps during GitLab onboarding and co-employer processes.
- Complete ad-hoc projects, reporting, and tasks.
- Conducting exit interviews for all Individual Contributors and collaborate closely with the People Business Partner team to support with administration related tasks.
- Issue traiging on a regular basis and label review/ adding.

#### People Operations Specialist (Intermediate) Requirements

- The ability to work autonomously and to drive your own performance & development would be important in this role.
- Prior extensive experience in an HR or People Operations role.
- Clear understanding of HR laws in one or multiple countries where GitLab is active.
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent).
- Excellent written and verbal communication skills.
- Exceptional customer service skills.
- Team player who can jump in and support the team on a variety of topics and tasks.
- Enthusiasm for and broad experience with software tools.
- Proven experience quickly learning new software tools.
- Willing to work with git and GitLab whenever possible.
- Willing to make People Operations as open and transparent as possible.
- Proven organizational skills with high attention to detail and the ability to prioritize.
- You share our [values](/handbook/values/), and work in accordance with those values.
- The ability to work in a fast-paced environment with attention to detail is essential.
- High sense of urgency and accuracy.
- Bachelor's degree or 2 years related experience.
- Experience at a growth-stage tech company is preferred.
- Ability to use GitLab

### Senior People Operations Specialist

The Senior people Operations Specialist reports to the Manager, People Operations.

#### Senior People Operations Specialist Job Grade

The Senior People Operations Specialist is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior People Operations Specialist Responsibilities

- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Partner with Legal, Tax and Compliance on international employment and contractual requirements.
- Maintain and improve program management including rollout, communication, reporting and metrics of Organizational health, Engagment Survey, DIB related surveys, 360 feedback cycle and Performance/ Potential Matrix Assessment. 
- Maintain and improve the stay interview process (analysis and actions). 
- Leading engagement survey administration, implementation and reporting. 
- Review new locations and work with PEO’s to update contracts. Update handbook appropriately.
- Data retention tracking and compliance.
- Review anniversary gift feedback on an annual basis and work with the supplier to iterate on the experience and ordering process.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel
- Work on onboarding people experience, values alignment during onboarding and improving onboarding through continuous iteration.
- Conducting exit interviews for all Management and Senior Management team members and collaborate closely with the People Business Partner team to support with administration related tasks.
- Stay interviews for IC’s.
- Partner with the People Operations Fullstack Engineer on automation and removing mundane tasks from Onboarding.
- Iterate on the onboarding issue and collaborate with the overall team to keep improving onboarding.
- Work with the People Operations Fullstack Engineer on automation related options for call management and videco conferencing provider.
- Manage Vendor contracts.
- Renewing or ending of contracts and working closely with Procurement on negotiations, and vendor selections.
- Coordinate with Finance on PEO payroll issue.
- Complete ad-hoc projects, reporting, and tasks.
- Conducting exit interviews for all Managers, Directors and members of the Executive Team and collaborate closely with the People Business Partner team to support with administration related tasks.
- Maintaining issue traiging on a regular basis and label review/ adding and contribute to the efficiency of our issue tracker.

#### Senior People Operations Specialist Requirements

* Extends that of the People Operations Specialist (Intermediate) requirements.

## Manager, People Operations

The Manager, People Operations reports to the Senior Manager, People Operations.

#### Manager, People Operations Job Grade

The Manager, People Operations is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, People Operations Responsibilities

- Onboard, mentor, and grow the careers of all team members.
- Coach and mentor PeopleOps team to effectively address team member queries in line with our values.
- Work with Director, People Operations to shape a strategy that aligns and moves GitLab towards continued growth, innovation and improvement.
- Manage and  suggest improvements for program management including rollout, communication, reporting and metrics of Organizational health, Engagment Survey, DIB related surveys, 360 feedback cycle and Performance/ Potential Matrix Assessment. 
- Provide training and support to the Specialist group to address team member and leadership queries effectively and timely.
- Oversee the co-employer relationships and act as the main point of contact for the People Group.
- Seek and review potential blockers of various People Operations processes and ensuring improvement on daily tasks and suggest automation where needed.
- Review and improve Slack answers and continue to train and mentor the team to update information in the handbook, for easy access to all.
- Manage and maintain an SLA to track response times for email queries across timezones.
- Continue to drive automation for easy access of employment confirmation letters, automatic invitations to Contribute, confirmation of business insurance, etc.
- Continuous managing and improvement of both administration and people experience during onboarding.
- Partnering closely with Managers at GitLab to gain feedback from various teams about onboarding, offboarding and transition issues.
- Review and report on People Operations and Onboarding metrics, including [OSAT](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) and [Onboarding Task Completion](/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd).
- Continuous improvement of offboarding for both voluntary and involuntary terms.
- Along with the People Business Partners, create a Terminations playbook to ensure the team can remain async and scale.
- Work closely with payroll, SecOps, and ITOps to continue to improve the administration of offboarding the time it takes to complete.
- Mananging issue traiging and label review/ adding and reviewing contributions to the efficiency of our issue tracker.
- Drive continued automation and efficiency to enhance the employee experience and maintain our efficiency value.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel
- Leading positive employee experience throughout their lifecycle with GitLab.
- Implementation and monthly report on trends from Stay Interviews.
- Quarterly report on trends from exit interview data.
- Manage vendor renewals and agreements.
- Partner with Finance Business Partner for budgeting purposes.

#### Manager, People Operations Requirements

- Ability to use GitLab
- The ability to work autonomously and to drive your own performance & development would be important in this role
- Prior extensive experience in a People Operations role
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Exceptional customer service skills
- team player who can jump in and support the team on a variety of topics and tasks
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Proven organizational skills with high attention to detail and the ability to prioritize
- You share our values, and work in accordance with those values
- The ability to work in a fast-paced environment with attention to detail is essential
- High sense of urgency and accuracy
- Experience at a growth-stage tech company

### Performance Indicators

- 12 month team member retention
- 12 month voluntary team member turnover
- Onboarding Satisfaction Survey > 4.5[](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat)
- Onboarding task completion < X (TBD)

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
- After that, candidates will be invited to interview with the Director of People Operations
- Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Senior Manager, People Operations

The Senior Manager, People Operations reports to Director, People Operations. 

#### Senior Manager, People Operations Job Grade

The Senior Manager, People Operations is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, People Operations Responsibilities

- Onboard, mentor, and grow the careers of all team members
- Provide coaching to improve performance of team members and drive accountability
- Work with Director, People Operations to shape a strategy that aligns and moves GitLab towards continued growth, innovation and improvement
- Provide training and support to the Specialist group to address team member and leadership queries effectively and timely
- Oversee the co-employer relationships and act as the main point of contact for the People Group
- Seek and review potential blockers of various People Operations processes and ensuring improvement on daily tasks and suggest automation where needed
- Improve Slack answers and continue to train and mentor the team to update information in the handbook, for easy access to all
- Implement an SLA to track response times for email queries across timezones
- Continue to drive automation for easy access of employment confirmation letters, automatic invitations to Contribute, confirmation of business insurance, etc
- Implementation of data retention strategies and partnering with People Leads to ensure this is implemented across all team member data consistently
- Continuous Improvement of both administration and people experience during onboarding.
- Partnering closely with Managers at GitLab to gain feedback from various teams about onboarding, offboarding and transition issues
- Review and report on People Operations and Onboarding metrics, including [OSAT](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) and [Onboarding Task Completion](/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd)
- Continuous improvement of offboarding for both voluntary and involuntary terms
- Along with the People Business Partners, create a Terminations playbook to ensure the team can remain async and scale
- Work closely with payroll, SecOps, and ITOps to continue to improve the administration of offboarding the time it takes to complete
- Mananging issue traiging and label review/ adding and reviewing contributions to the efficiency of our issue tracker. Provide strategic guidance and coaching on more difficult discussions and collaboration within issues. 

#### Senior Manager, People Operations Requirements

- Ability to use GitLab
- The ability to work autonomously and to drive your own performance & development would be important in this role
- Prior extensive experience in a People Operations role
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Exceptional customer service skills
- Team player who can jump in and support the team on a variety of topics and tasks
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Proven organizational skills with high attention to detail and the ability to prioritize
- You share our values, and work in accordance with those values
- The ability to work in a fast-paced environment with attention to detail is essential
- High sense of urgency and accuracy
- Experience at a growth-stage tech company

### Performance Indicators

- [12 month team member retention](/handbook/people-group/people-success-performance-indicators/#team-member-retention-rolling-12-months)
- [12 month voluntary team member turnover](/handbook/people-group/people-success-performance-indicators/#team-member-voluntary-retention-rolling-12-months)
- Onboarding Satisfaction Survey > 4.5[](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat)
- Onboarding task completion < X (TBD)

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
* After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
* After that, candidates will be invited to interview with the Director of People Operations
* Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Director, Global People Operations

The Director of Global People Operations reports to the [Senior Director, People Success](/job-families/people-ops/people-leadership/).

#### Director, Global People Operations Job Grade

The Director, Global People Operations is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Global People Operations Responsibilities

- Lead and manage a growing People Operations team responsible for managing the employee experience throughout the employee lifecycle.
- Help shape a global People Operations strategy that aligns and moves the business towards continued growth, innovation and improvement.
- Provide HR support and consultation to the business; answering employee and manager questions about HR programs, policies, and other HR-related items.
- Provide HR support and leadership with respect to day-to-day HR life cycle activities of client groups, including performance management issues, investigations, and reorganizations.
- Understand workforce needs as the company scales managing costs while staying competitive with salary, benefits, perks, leaves, etc.
- Work with the executive team to come up with a competitive compensation strategy.
- Assess and apply market data, ensuring alignment with the company’s total compensation philosophy.
- Oversee performance, development, and the compensation review process.
- Drive a progressive, proactive, positive culture, and increase levels of engagement, enablement, and retention.
- Coach, influence, and provide guidance to business partners to figure out optimal organizational design, development, and performance management plans.
- Assist with the maintenance and accuracy of HR data, including processing SAP data, such as Employee Action Forms.
- Provide resolution to employee and organizational issues in a proactive and sensitive manner.
- Assess/identify HR strategy, policy, or process improvements.
- Ensure People Operations strategies and processes remain aligned with the company’s talent management and workforce plans to enhance employee engagement and sustain business growth.
- Collaborate with Chief People Officer, People Operations, and organizational leaders in fostering a culture of empowerment and performance; establish the employee lifecycle journey and succession plans.
- Maintain in-depth knowledge of local, global, and federal employment laws; maintain and store records judiciously and securely.

#### Director, Global People Operations Requirements

- 8+ years of progressive experience in HR roles with a demonstrable track record of building and optimizing processes, systems, and structures.
- Requires a Bachelor’s degree preferably in Human Resources, Organizational Development, Organizational Leadership, or related field.
- 5+ years hands-on experience with compensation & benefits.
- Labor Relations experience required, preferably in multiple countries.
- Working knowledge of regulatory and legal requirements related to total rewards and systems.
- 3-5 years of experience leading people and cross-functional organizations.
- Demonstrable ability to own, execute and deliver on short- and long-term projects.
- Strategic and innovative thinker; able to prioritize and use sound judgment and decision-making.
- Executive presence with excellent written and oral communication skills.
- Business insight and high EQ to successfully collaborate with executives and business partners at all levels.
- Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks).
- You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
- Ability to use GitLab

## Specialities 

### Team Member Relations Specialist

Owns the Team members relations process from intake to conclusion, including coaching, research/investigations, documents and makes recommendations on appropriate response or action needed.  

#### Team Member Relations Specialist Responsibilities

- **Disciplinary** :  Partner with People Business Partners and other teams and people leaders to ensure that investigations/Disciplinary processes are handled in a fair, timely manner consistent with local requirements including all documentation.
- **Coaching** : Provide Team Member Relations support, thought partnership, and coaching for all team members and levels of management in the organization on issues including absenteeeism, performance, conduct and harrassment.  
- **Compliance** : Parnter with Legal and People Compliance to ensuring compliance with policies, practices and applicable employment legislation.
- **Policy and Process Management** : Balance a broad range of tasks including the interpretation and development of policies and procedures, facilitiating training on key Team Member Relations issues, and contributing to the development and implementation of Team Member Relations programs based on line of business goals and People Success strategy.
- **Process Improvement** : Identifying process improvement opportunities and policy gaps through trend anaylsis.  

#### Team Member Relations Specialist Requirements

- Effective communication and problem solving skills
- Demonstrates empathy and experience driving inclusive culture
- Minimum of 2 year global experience managing team member relation cases
- Demonstrated solid judgement and experience assisting risk relative to the business
- Demonstratees project management, change management, and experience driving programs independently
- Experience learning and thriving in a constantly changing Global environment and to cultivate relationships across Global teams
- Be a trusted adviser by providing consultation and resolution guidance to promote a positive GitLab culture

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our Senior Manager, People Operations
* After that, candidates will be invited to schedule a 30 minute interview with members of the People Business Partner Team 
* Next, candidates will be invited to schedule a 30 minute interview with members of the Legal team
* After that, candidates will be invited to interview with the Senior Director, People Success
* Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Career Ladder

The next step in the People Operations job family is to move to the [People Leadership job family](/job-families/people-ops/people-leadership/).
