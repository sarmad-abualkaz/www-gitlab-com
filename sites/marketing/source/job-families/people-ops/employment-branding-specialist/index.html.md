---
layout: job_family_page
title: "Talent Brand"
---

## Senior Talent Brand Manager

The Senior Talent Brand Manager will help advance our recruitment and talent brand. They will work cross-functionally to tell the story of what it's like to work at GitLab and build our reputation as an extraordinary workplace. They will play an important role through their experience in building content for brands across social channels and other platforms to help enable our rapid growth.

### Job Grade 

The Senior Talent Brand Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Build talent brand programs, operationalize them, and measure their success
- Collaborate with key stakeholders to develop GitLab's talent brand
- Monitor and grow our brand presence across key talent-facing channels (jobs site, LinkedIn, Glassdoor, etc.)
- Partner with the People Operations team and key stakeholders globally to develop and execute strategies that enhance GitLab’s visibility in key recruiting markets
- Evolve GitLab’s talent brand strategy to effectively articulate GitLab’s culture and value proposition to attract top talent
- Provide strategy and support for related programs such as GitLab's diversity, inclusion and belonging
- Collaborate with internal GitLab teams, particularly Marketing, on key projects (social media, jobs site, events, remote work, PR, internal communications) to ensure consistency in messaging and approach
- Develop recruitment marketing campaigns to increase awareness of GitLab's talent brand with potential candidates and help the Recruiting team meet hiring plan goals
- Enhance the look and feel of communications (internal and external) throughout the candidate and team member lifecycle
- Define our approach for employer awards and lists that will help GitLab be recognized as an employer of choice; complete applications and manage related internal surveys for these awards
- Create recruiter enablement tools to assist the recruiting team in sharing the GitLab talent brand with candidates 

### Requirements

- Bachelor’s degree and at least 3 years of Marketing or Human Resources experience
- Prior marketing, brand, and social media experience (preferably within recruiting or talent brand)
- Demonstrated ability to deliver targeted recruitment marketing, social media, and employee value proposition building strategies
- A natural storyteller with excellent narration and writing skills
- Ability to navigate cultural differences and build global but locally relevant solutions
- Strong social and communication skills (verbal and written), across all levels
- Excellent organizational skills, time management, and priority setting
- Deadline oriented, and able to work in a fast-paced environment with ever-changing priorities
- Self-motivated with the ability to work both independently and collaboratively
- Proficient in Google Docs
- You share our [values](/handbook/values/), and work in accordance with those values
- Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks)
- Ability to use GitLab

## Performance Indicators

- [Glassdoor engagement](/handbook/people-group/employment-branding/#glassdoor-engagement)
- [LinkedIn Talent Brand metrics](/handbook/people-group/employment-branding/#linkedin-talent-brand-metrics)
- [Team member engagement score](/handbook/people-group/employment-branding/#team-member-engagement-score)
- [Team member voluntary turnover](/handbook/people-group/people-group-metrics/#team-member-voluntary-turnover)
- [Hires vs. plan](/handbook/hiring/metrics/#hires-vs-plan)
- [Social Referrals](https://about.gitlab.com/handbook/hiring/metrics/#social-referrals)

## Hiring Process

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, candidates will be invited to schedule a 30 minute interview with one of our Senior Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Recruiting Director
- After that, candidates will be invited to schedule a 30 minute interview with our Chief People Officer
- Next, the candidate will be invited to interview with a People Business Partner
- After that, the candidate will be invited to interview with a Marketing Manager
- Finally, our CEO may choose to conduct a final interview

### Career Ladder

The next step in the Talent Brand job family is to move a management role which is not yet defined at GitLab. 

