---
layout: handbook-page-toc
title: "Demandbase"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Demandbase
Demandbase is a complete end-to-end solution for [Account-Based Marketing](/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/). We primarily use Demandbase as a targeting and personalization platform to target online ads to companies that fit our ICP and tiered account criteria. Demandbase also has a wealth of intent and propensity to buy data that is married with account engagement indicators to create a wholistic intent maping for each account. 

We compile the intent data by building audiences, groups of target accounts with the most potential to purchase based on our buyer criteria, which we can then leverage for use throughout the funnel in advertising and SDR/sales follow-up. Demandbase also delivers ongoing signals around behaviors and intent to keep our list up to date. This information helps both marketing and sales focus our efforts on the right accounts. 

## Use cases and features
To date, we use the following Demandbase features. In addition to the general training resources section below, each use case will also have a section on this page with more information and training resources. 
- [ABM platform](https://support.demandbase.com/hc/en-us/sections/360003540751-ABM-Platform-Solution): identify the companies that meet your buying criteria and are showing the right intent signals
- [Targeting](https://support.demandbase.com/hc/en-us/sections/360009465252-Getting-Started-with-Self-Serve-Targeting): Reach the full buying committee and nurture your best-fit accounts from awareness to revenue with account-based advertising. Easily set up, launch, and manage campaigns.
- [Conversion](https://support.demandbase.com/hc/en-us/sections/360003566352-Conversion-Solution): give SDR and Sales teams the data and insights they need to better understand and target the right individuals, with personalized messages, within their target accounts, so they can close deals faster.
- Analytics: 
     - [ABM analytics](https://support.demandbase.com/hc/en-us/sections/360003540851-ABM-Analytics-Solution): focus on business outcomes and report on account-level reach, engagement, and sales outcomes.
     - [Site analytics](https://support.demandbase.com/hc/en-us/sections/360009288811-Site-Analytics-Solution): provide insight around channel performance (i.e. - display advertising, traffic, campaign influence) 

## General Demandbase training resources
- Demandbase Certification: Go to the [Demandbase training portal](https://training.demandbase.com/) > navigate to ‘Enroll in Certifications’ > ‘Demandbase Solutions Certification’
- [Training resources library](https://support.demandbase.com/hc/en-us/categories/360001449332-Learn-to-Use): videos and articles for getting started with Demandbase. 
- [Demandbase glossary](https://support.demandbase.com/hc/en-us/articles/360022927111-Demandbase-Glossary): list of key terms and their definitions.
- [Introduction to ABM](https://support.demandbase.com/hc/en-us/articles/360037346672-ABM-Introduction): what is ABM? 

## Demandbase ABM Platform
The [Demandbase ABM Platform](https://support.demandbase.com/hc/en-us/sections/360003540751-ABM-Platform-Solution) lets you discover and manage audiences of target accounts, measure the progress of those accounts and act on them across the entire funnel.

### Account Lists
An `Account list` (called `audiences` in Demandbase Classic) is a collection of accounts based on a list of qualifications, similar to a report in Salesforce.  An account must be in an account list in order for us to take any action on that account in Demandbase. An account can belong to as many lists as we want ot build.  There are several ways to build an account list in Demandbase (in order of recommended):
- Assigned account lists: these lists are pre-built for users in DB based on their assigned account in Salesforce.  The following groups have accounts lists:
   - Field Marketing Managers
   - SDR
   - Sales
- Dynamic list using `selectors`
- Salesforce report (must be an account report, not opportunity or campaign, etc)
- ~~Static list of account through a CSV upload~~.  We do not recommend using this method as it is a signle point in time uplaod and not easily modified.

#### Account List naming convention:
The name of an audience should tell you who the DRI is (team) and what the list is for.  Audience names are editable so if you are unsure, please name using the following criteria and reach out to the ABM team on slack.

**Example: (FM) 20200901_SecurityWorkshop**

1. Team DRI for the list should be identified by the following:
- (ABM) Account Based Marketing & Strategy
- (FM) Field Marketing
- (DG) Demand Generation
- All others- please ping the ABM team in the #abmteam slack channel so we can add a naming convention for you
2. Name of the list- what does this list contain?
- campaign tag if it is an audience specific to a campaign
- intent based etc
- Segment- if an audience is a segment of a larger audience.  example: if you wanted to create a segment of this audience: (FM) 20200901_SecurityWorkshop it would be named (FM) Segment-20200901_SecurityWorkshop 

### Scoring Account in Demandbase
Demandbase One has two different methods of scoring accounts.  `Qualification Score` quantifies the liklihood of an account ever becoming a customer, and looks at firmographic data that we input.  `Pipeline Predict` quanitifes the liklihood of an account becoming an opportunity in the next 30 days based on a number of inputs, both firmographic and intent.

#### Qualification Score
Our qualification score is built based on our [ideal customer profile (ICP)](/handbook/marketing/revenue-marketing/account-based-strategy/ideal-customer-profile/) for Large accounts, although a future release from Demandbase One will allow us to create multiple scoring models.  This will score all accounts in the platform between 0-100% based on their match to the ICP.  This score does not change often.

#### Pipeline Predict 
Pipeline predict measure the propensity ot buy in the next 30 days eg accounts that are liekly to become an open opportunity.  This score changes often because it is based on both an account's qualification score and their buying signals (intent, interactions with sales, etc).

| Category | Description |
| ------ | ------ |
| Qualification score | matches our ICP |
| Website visitors | how many people are visitng our website from the company |
| DB Intent | onsite and offsite search from the company |
| Inbox data | interactions with Sales |
| CRM data (Salesforce) | meetings, demos, IQM |
| MAS data (Marketo) | interactions with our marketing campaigns |
| Advertising activity | engagement with our Demandbase campaigns |

### Engagement Minutes
Engagement minutes track the amount of time an account spends with GitLab. This allows us to aggregate all of the interactions an account, and all leads and contacts associated with that account, have with us.  This includes both Sales and Marketing engagement, as well as website visits and intent keywords searches online. Being able to combine all of these iteractions allows us to better predict who is liekyl to become a customer.

| Engagement Type | Description | 
| ------ | ------ | ------ |
| Website visits | both known and anonymous website visitors | 
| Marketing Automation (Marketo) | Email opens, event attendance, content downloads, etc. |
| Salesforce | Activities, tasks, and campaign membership |
| Email/Calendar | human interactions including email and meetings with Sales |
| Weighting | accelerators based on role i.e. a Director would receive a 25% increase in engagement minutes versus someone with an individual contributer role |

### Journey stages
Demandbase allows us to see where an account is within our funnel.  The account stages takes into consideration all lead, contact, and anonymous person activity and aggregates that data at the account level.  Our journey stages are set up as follows:

| Stage | Description |
| ------ | ------ |
| No engagement | Accounts that have no recorded engagement with GitLab |
| Qualified | Accounts with a qualification score of >70 |
| Aware | Intent strength = High activity date < 30 days |
| Engaged | Accounts with engagement >= 10 in the past 3 months |
| Marketing Qualified Account | Accounts with a pipeline predict score of >95 |
| Pre-Opportunity | Accounts with opps in stage 0 |
| Early Stage Opportunity | Accounts with opps in stage 1-2 |
| Mid Stage Opportunity | Accounts with opps in stage 3-4 |
| Late Stage Opportunity | Accounts with opps in stage 5-7 |
| Customer | All current customers |
| Lost Opportunities | Accounts with closed lost opps |

## Demandbase Advertising
[Demandbase Targeting Solution](https://support.demandbase.com/hc/en-us/sections/360009465252-Getting-Started-with-Self-Serve-Targeting) helps us reach the actual buyers in our target accounts with relevant ads on brand-safe sites to execute account-based marketing campaigns.
### Use Cases
Account-Based Advertising: Uses your target account list to help you display personalized advertising across many different channels.
Ad Personalization: Helps you to dynamically deliver personalized ads to selected accounts.
Keyword Discovery: Helps you discover new areas of intent for buyers at your target accounts.
Campaign Creation and Reporting: Helps you to set up ad campaigns and measure the results of your ad campaign against business outcomes. 
### Demandbase Display Campaigns
Demandbase has a programmatic display platform that enables personalized display advertising for target accounts using its own internal DSP. Demandbase Display ads are different from [the standard Paid Display ads](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#display-ads) we typically run in Google Display Network for demand generation. Display campaigns excel in reach, brand awareness, and engagement, so coupled with Demandbase's account-centric targeting, DB campaign objectives expand to include targeted account penetration.
 We partner directly with Demandbase and do not run these campaigns through PMG, our paid advertising digital agency.

### Demandbase Campaign Process
If you would like to target named accounts with paid display ads, create an issue with the appropriate Demandbase Campaign Request template:
- [Campaign Request issue template for Field and Corporate Marketing](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=Demandbase_Campaign_Request)
- [General Campaign Request issue template (for all other teams)](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/new?issuable_template=Demandbase_Campaign_Request_Template)

The DMP will then conduct a reach test with Demandbase to determine if your selected accounts are reachable, so please provide estimated budget, campaign duration, geo target, and a list of accounts [formatted for DB (Demandbase)](https://docs.google.com/spreadsheets/d/1sJvmzdhL8k5N6WQPSlE3ndAF0jL8MUTuZcD6dM-1D0w/edit?usp=sharing). If your reach test comes back too low, the DMP will recommend that you add more accounts, expand your geo target, increase your campaign duration, and/or double check to make sure your list is formatted correctly for Demandbase upload. After the reach test confirms reachable accounts and available display inventory, the DMP will set up the campaign in Demandbase by selecting the audience, layering on geo targeting, and uploading the display images & landing URL(s). Once the campaign is submitted, it will go live within 1-2 business days.

If your campaign objective is promoting an event or webcast, we recommend launching one month in advance of the event date in order to increase awareness & reach. If your campaign objective is account penetration, we recommend running your campaign as “always-on” to build awareness for your priority accounts and optimize over time with relevant assets. The DMP will provide a UTM report to track inquiries/registrations attributed to your DB campaign. Front-end Demandbase metric reports are also available upon request.


### Demandbase Ads
#### Display Ad Specs
- 728x90
- 300x250
- 160x600
- 300x600
- 970x250
- 320x50 
- 300x50

#### Display Ad Copy
- **Top of funnel:** Use a broad value proposition to increase awareness and education, assert your brand value and why prospects should care.
- **Middle of funnel:** Include a tailored value proposition, provide practical how-to content, best practices, and tips and tricks.
- **Bottom of funnel:** Reinforce the value proposition, go into further detail about your offerings with case studies, ROI calculators, and product and solution content.
- **All stages:** Keep it simple and have a clear CTA.

#### Demandbase Campaign Best Practices
##### Campaign Run Time:
- Event promotion: 1 month minimum
- Target account penetration: 2-3 months minimum
- Longer advertising campaigns that span the full funnel can help build awareness and create more engagement. The result is a higher number of accounts that go into the pipeline with a higher average booking value.

##### Budget:
- [Demandbase funnel budgeting](https://docs.google.com/presentation/d/1swa9gWFAttRAas5feGSWPgsbx1HyUwiNtTXYiOL47wE/edit#slide=id.g810b16a717_0_0)
 - Start with a low monthly budget for top of funnel campaigns and increase the budget as you progress to mid and lower funnel campaigns.

##### Audiences:
- You can segment by stage in the buyer’s journey, company size, industry, etc. The core message for each of these groups is different, therefore they shouldn’t receive the same ad creative.
- Segment audiences into High, Medium, and Low intent based on intent data results

### Digital Campaign Metrics & Performance
- **Impressions:** Total ad views served
- **Clicks:** Total ad clicks
- **CTR:** Click-through rate. -> Formula: Clicks divided by Impressions
- **CPM:** Average cost per one thousand impressions
- **CPC:** Cost per click. -> Formula: Cost divided by Clicks
- Top Publishers
- Top Performing Creative
- Top Engaged Accounts

Since Demandbase is an ABM-oriented DSP, it self-optimizes for on-site engagement rather than traditional display metrics like CTR. We can check engagement metrics after two weeks, once the campaign has stabilized from initial launch, similar to learning phases in paid social. 

Budget pacing is spread out based on campaign duration, but can rise or dip based on inventory. It will ultimately hit the budget with no overages in the end.

### Demandbase Campaigns for Field Marketing Promotions
Our Field Marketing team uses Deamandbase to heat up (either continuing to build brand awareness or starting to build that essential awareness) accounts on our GL4300 & MM4000 list and to drive specifc registrations to our programs. 

To use Demandbase solely for event promotion can increase brand lift for accounts, but it won't match the volume of registrations paid social can generate. Due to registration inefficiencies by using Demandbase only for promotions, we recommend using Demandbase to support & enhance account lead gen tactics (paid social, email, etc) with brand awareness, and gather account activity data to measure account penetration. We recommend either running Demandbase in conjunction with lead gen tactics, or running standalone ongoing Demandbase campaigns to warm up audiences before targeting the same audience with lead gen ads for events. Both recommendations allow us to continuously gather data in order to form smarter campaigns down the line.

We suggest using ABM Analytics & Account Stage reporting to show account engagement & influence. Although brand lift can be difficult to measure, you can see how accounts advanced through the funnel during Demandbase Display campaigns. For Field Marketers, ongoing training can help this team utilize Demandbase in order to parse account data & help with regional strategy.

## Analytics 
### Demandbase ABM Analytics 
[ABM Analytics](https://support.demandbase.com/hc/en-us/articles/360005054311-ABM-Analytics-Overview) is a native analytics tool within the Demandbase platform. It gives you insight into how your target accounts are performing across your full marketing funnel from engagement to conversion to closed won. 

#### How to use ABM Analytics
- [Working with the ABM Analytics Dashboard](https://support.demandbase.com/hc/en-us/articles/360005054311-ABM-Analytics-Overview#h_5966557921550945944419)
- [Steps for Using ABM Analytics](https://support.demandbase.com/hc/en-us/articles/360005054311-ABM-Analytics-Overview#h_578082746371550945977821)

#### Opportunity Reports Manager
The [Opportunity Reports Manager](https://support.demandbase.com/hc/en-us/articles/360036023532-Working-with-Opportunity-Reports-Manager) allows you to customize which opportunities are used in your reports and analytics within ABM Analytics. To date, you can only filter/segment by opportunity stage/type/status. 

Our Demandbase instance already has a report for each opportunity stage. This lets you select an audience (list of accounts) and ‘filter’ it by opportunity stage. 

##### How to filter using the Opportunity Reports feature
1. Create an audience or leverage one of the audiences already in our DB instance that has all of the accounts you want to see.
2. In DB, navigate to the 'ABM Analytics' tab.
3. Select the audience you want to see the data for from the 'primary audience' dropdown.
4. To narrow the audience by opp stage, check the box 'Filter results by Opportunity Report' and select the opp stage from the dropdown.

#### ABM Metrics
- **Lifted Accounts**: Percentage of the target accounts that have more engagement (page views) during the campaign(s) compared to the baseline period of 30-days prior to the start of the campaign(s). Baseline page view counts are normalized for campaign length in calculating this metric.
- **Page Views:** Total page views on your website during the campaign(s)
- **% increase in Page Views:** Percent change in page views during the campaign(s), compared to baseline period of the 30 days preceding the start of the campaign. *Note: Baseline page view counts are normalized for campaign length in calculating this metric.
- **Account Performance by Stage:**
     - **Total Accounts:** The total number of accounts being targeted
     - **Reach:** he total number of accounts that have been served at least one impression
     - **Visited:** The total number of accounts that have been on site during the campaign(s)
     - **Clicked:** The total number of accounts from which clicks have been generated during the campaign(s)
     - **Engaged:** The total number of targeted accounts that have had three or more unique sessions within a 30-day period.
     - **Converted:** TBD
     - **Opportunity:** The total number of accounts with at least one new CRM opportunity created during the campaign(s)
     - **Won:** The total number of accounts with at least one CRM opportunity that has progressed to Closed/Won during the campaign(s)

### Demandbase Site Analytics
Demandbase Site Analytics gives you website analytics with an account-based lens so you can better evaluate website performance and personalize marketing efforts to them. 

#### Use Cases
- Create and target specific audiences with relevant content
- Gain insights on which site content and web pages are most valued by visitors from your target accounts
- Use UTM and URL parameters to track channel and campaign performance over time
- Build campaigns around specific levels of website activity
- Drive registration for upcoming events: discover who has visited your event pages, but has not registered for an event. [4 min video](https://www.youtube.com/watch?v=4JaoI2MlJ_s) on how GitLab Field Marketers should be using Site Analytics to help drive registrations. Note: this video is private to GitLab team members only, as we are showing real data in our SFDC instance.  

#### How to use Site Analytics
- Steps for using Site Analtyics as well as building an audience from Site Analytics can be found [here](https://support.demandbase.com/hc/en-us/articles/360039832352-Working-with-Site-Analytics#h_8ef9f39f-0bca-4f8e-babe-01225c1abee1). 

## Demandbase & Salesforce 
#### Salesforce Metrics

- **[SAO](/handbook/sales/field-operations/gtm-resources/#glossary): Sales Accepted Opportunity:** Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting
- **[Closed Won](/handbook/sales/field-operations/gtm-resources/#opportunity-stages):** The terms have been agreed to by both parties and the quote has been approved by Finance.

### Salesforce intent fields & definitions

We are in the process of building out the respective views for Sales, SDR, and FMM in Salesforce and more infromation will be documented here once that process is complete.

## Running campaigns in Demandbase
The ABS team is responsible for executing and managing any campaign in Demandbase.  As part of the campaign request process, you need to have a media order signed for the campaign budget.  Directions are below based on your role.

#### Field Marketing campaigns
Media orders are signed on a quarterly basis for global field marketing.  The ABS team will open an issue to request the amount from the field marketing team and open the finance issue.  From there, field marketing is responsible for getting all of the appropriate approvals required by procurement based on the process documented [here](https://about.gitlab.com/handbook/finance/authorization-matrix/).  Once the order is countersigned and is live in the platform, it will be added to the master order in Demandbase for field marketing.  This is the order every field marketing campaign will be executed on.
- **month end procedures**
     At the end of each month, we are required for finance to document the actuals of each campaign spent.  This will be done by the ABS team, with visibility to Field Marketing.
- **additional budget mid quarter**
     We can always sign additional orders mid quarter, just be aware that budget availability is based on SLA's with procurement and will take roughly 48 hours from the time we deliver the counter signed agreement to Demandbase to be available in the platform.
- **what if a campaign doesn't spend as expected?**
     At the end of the campaign if there is budget leftover (this typically happens if availability changes) we can extend the campaign duration or zero out the budget in Demandbase, meaning that amount will go back in to the Field Marketing bank.
- **unspent budget EOQ** 
     If there is budget leftover that was not spent at end of quarter, that ammount will be deducted from the following quarter's order since we have commited to Demandbase to spend that money.
