---
layout: markdown_page
title: How to Perform Customer Emergencies Duties
category: On-call
description: "Describes the role and responsibilities for Customer Emergencies rotation in Support Engineering"
---

- TOC
{:toc}

----

## Introduction

This is a placeholder - please use the [current on-call handbook](/handbook/support/on-call/)

## Things To Know

### How Are Incidents Declared?

### What to do if you don't know what to say

### Reviewing Past Incidents

### Stage 1: **Incident Creation**

#### PagerDuty Status

{:.no_toc}

- **Triggered** - "An incident exists that requires the attention of a CMOC"
- **Acknowledged** - "I have seen the page and am on my way to the incident room"
- **Resolved** - "A tracking issue has been created, the status page has been updated and I am actively engaged in the incident management process"

**NB:** "Resolved" in PagerDuty does not mean the underlying issue has been resolved.

#### 1. Join The Incident Zoom Call

#### 2. Create the Incident

#### 3. Notify Account Management team

### Stage 2: **Incident Updates**

### Stage 3: **Incident Resolution**

#### Monitoring

#### Resolved

#### Post-Mortem

## Handover Procedure

## Customer Emergencies Shadow PagerDuty Schedule
