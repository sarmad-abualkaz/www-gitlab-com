---
layout: handbook-page-toc
title: "Safeguard"
description: "The following benefits are provided by Safeguard and apply to team members who are contracted through Safeguard."
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [Safeguard](https://www.safeguardglobal.com/) and apply to team members who are contracted through Safeguard. If there are any questions, these should be directed to the Total Reward team at GitLab who will then contact the appropriate individual at Safeguard.

## Brazil
* Medical and dental insurances are provided by the local provider, Operadora Bradesco Saúde.
* Both the company and team members make contributions to the National Institute of Social Security which covers benefits such as retirement, pension, illness, disability and unemployment. These social security contributions are included as part of Safeguard’s payroll process.

## France
Social security contributions towards pension, disability and health insurance are made via Safeguard’s payroll process.

## Italy
* Medical - Team members have access to government-subsidized healthcare as well as additional insurance called QU.A.S. The annual cost for QU.A.S. is 56 EUR paid by the team member and 350 EUR paid by the company.

* Pension and Life Insurance - As part of Safeguard payroll, mandatory contributions are made towards social security funds.
Quadri & Impiegati Employees have the possibility to join the complementary pension fund Fon.te. The optional employee contribution of 0,55% will be matched by an additional employer contribution of 1,55%. Dirigenti (Managers) the NCA grants private pension benefits through the funds Mario Negri.


## Spain

_We are currently unable to hire any more employees or contractors in Spain. Please see [Country Hiring Guidelines](/jobs/faq/#country-hiring-guidelines) for more information._

- Currently Safeguard does not provide private healthcare
- Accruals for 13th and 14th month salaries
- General risks and unemployment insurance
- Salary guarantee fund (FOGASA)
- Work accident insurance

 GitLab currently has no plans to offer Life Insurance, Private Medical cover, or a private pension due to the government cover in Spain. If GitLab did want to look into offering such benefits through Safeguard, the employees would need to subscribe themselves to the insurance companies and they would be paid for it in the form of allowance on a monthly basis covering the price that the insurance company requests; this would then be billed to Gitlab.


## Other countries

Safeguard uses multiple third parties across the globe to assist in locations where they do not have a direct payroll.


## Switzerland

As per social security obligations, for its part, the employer pays:
- half of the total premium of the AVS/AI/APG
- the whole of the accident insurance contribution (professional)
- half of the loss of earnings insurance in the event of sickness
- half the total unemployment benefit insurance premium
- half the total maternity insurance premium
- part of training and execution
- LPP Silver
- During the execution of a mission at a client of the employer, the employee is insured against the risks of occupational accidents at the CNA. If it works 8 hours or more per week, it is also insured against the risks of nonprofessional accidents at the SUVA, according to the legal provisions in force. The SUVA benefits replace the obligation to pay wages according to art. 32a CO. The waiting period is 3 days from the accident.
- Employees are entitled to maternity benefits, in accordance with article 16b of the law on allowances for losses of earnings, LAPG, if they have been compulsorily insured within the meaning of the old-age and survivors insurance, LAVS, during the 9 months preceding the childbirth and that during this period, they were in gainful employment lasting 5 months and are still paid at the date of childbirth. The right to the allowance takes effect from the day of childbirth. The mother benefits from maternity leave up to a maximum of 14 weeks paid at a rate of 80% of the average earnings for the activity carried out before childbirth. The benefit is paid in the form of a daily subsistence allowance (max. 98 daily subsistence allowances). If the mother takes up her gainful employment again during this period, the right is extinguished early.
