---
layout: handbook-page-toc
title: "GitLab LTD (UK) Benefits"
description: "GitLab LTD (UK) benefits specific to UK based team members."
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to UK based employees

For any queries related to UK benefits, please email `total-rewards@gitlab.com` for further assistance. 

## Medical Insurance

For UK based employees GitLab provides paid membership of medical insurance with AXA PPP. This private medical insurance level is named Business Health Select with core cover. Family members can be added to the insurance and GitLab will pay 66% of the cost to include them. Further information can also be found in the [AXA PPP Brochure](https://drive.google.com/file/d/1iaxamYvMcn4G6tWdmnhiiCR_HAtGKVcN/view?usp=sharing).

Please let the Total Rewards team know if you would like to join the scheme via email to `total-rewards@gitlab.com`. Include the full names and date of birth of all dependants you would like added to the scheme. The Total Rewards team will forward your request to our UK HR partners by email. Please note that it can take 7 - 10 working days to receive a reply and confirmation from AXA PPP. 

FYI Total Rewards team: The UK HR partner's contact information can be found in the Entity and Co-Employer HR Contacts note in the PeopleOps vault in 1Password.

The current coverage includes:

- Full outpatient cover
- £0 Excess (meaning that employees are fully covered)
- Extra covers (all included)
   - Therapies cover option
   - Mental Health cover option
   - Extra Care option includes hospital at home, cash benefit, oral surgery, chiropody
   - Extra Cancer care option
- Virtual GP appointments – [DR@Hand](https://drive.google.com/file/d/1YzkeG0qMLPLi6TEY6mQXqV8xooMQb4_Z/view?usp=sharing)

- **Note:** Optical and dentist cashback option **is not** included.

Please also note that this is a taxable benefit. The underwriting is on a Moratorium basis, meaning that team members will not have cover for treatment of medical problems they may have had in the five years before joining. Please refer to page 14 in the AXA PPP Brochure for more information.

### Obtaining a Medical Insurance Quote

During Vistra's initial onboarding email to enroll in the scheme, they will provide a quote for a team member to add coverage and an estimate for adding dependents. The cost of dependendent cover can be estimated as follows:

| Tier | Multiplier |
|------|------------|
| Team Member + Spouse | 2x Team Member Premium |
| Team Member + Child(ren) | 1.6x Team Member Premium |
| Family | 2.5x Team Member Premium |

### Vision 

As GitLab team members work (DSE) display screens equipment on a regular basis, GitLab will reimburse team members for a full eye examination and vision test up to 30 GBP. 

If the test shows that the team member needs corrective lenses exclusively for DSE work, GitLab will reimburse towards the cost of a basic pair of frames and lenses (i.e. not designer lenses) up to 120 GBP.

The team member needs to provide documentation (a prescription) from the optician that confirms if the lenses are specifically required for reading a display screen only (this is normally a very small proportion of users) and not required for general day to day use. Please submit your receipts via expensify and prescription to Total Rewards. 

GitLab will explore adding vision coverage as a future iteration to our health benefits 

## Life Insurance

GitLab is currently reviewing implementing life insurance and disability in the following [issue](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/44).

## Pension Introduction

GitLab provides and contributes into an Auto-Enrollment personal pension scheme with [Scottish Widows](http://www.scottishwidows.co.uk/retirement/retirement-explained/basics/auto-enrolment/). GitLab is also working with [Oakley Financial](http://www.oakleyfinancial.com/) who are able to provide help and guidance should employees have any questions about the pension scheme. An email and telephone service is available and you can find the details in the [contacts](#contacts) section.

### Salary Sacrifice

From November 2019 GitLab will be offering the option to contribute to your Company Pension Scheme via Salary Exchange. As part of this Salary Exchange offering GitLab will be passing on their employer NI saving to you as an employee in the form of an increased pension contribution. You can find more information in the following [document](https://drive.google.com/file/d/1raP4uLizlTIBxZBX7q0h-op9yhWFVOYT/view?usp=sharing) detailing the benefits and potential disadvantages of changing to a Salary Exchange arrangement.

**Salary Sacrifice FAQ**
* Can we change the amount during the period we're contracted for salary exchange?
    * Yes it is possible to change the amount/increase your employee percent; Vistra will issue a new letter with a new amount.
* What happens if our 'actual' salary changes during the period?
    * Your employee contribution is a percent of basic pay so if your basic pay increase the pension contribution will too.
* What happens if we receive a discretionary bonus?
    * Your definition of pensionable pay is basic salary the percent will not automatically be applied to the bonus. Should you wish to contribute part of you bonus to your pension pot you can do that.
* Is it possible to start from any month?
    * Yes it is; it would be processed on the next available payroll.
* How do I make changes to my salary sacrifice amount?
    * Email total-rewards@gitlab.com with the new percentage of your salary that you would like to sacrifice.

### Auto-Enrollment

Auto-enrollment is a scheme brought in by the Government as they want everyone to save more for their retirement. Employees will receive a letter from GitLab informing them when they have been auto enrolled in the scheme. As this is now law, employees will be auto enrolled into the GitLab UK Group Personal Pension Scheme from January 1, 2018 or as soon as they join the company if:

- Earnings are over £10,000 a year
- Employees are aged 22 or over; and
- Are under the state pension age

If an employee does not meet these criteria then they will be considered an **Entitled Worker** (earn less than £5,824 p.a) or a **Non-Eligible Jobholder** (earn between £5,824 and £10,000 p.a).

- **Entitled workers** must be offered the opportunity to join the pension scheme. They can make contributions to the pension scheme if they choose. However, there is no requirement for GitLab to make a contribution
- **Non-Eligible jobholders** must be offered the opportunity to opt in to an automatic enrollment scheme.

### Scottish Widows Group Personal Pension Scheme

The following process is followed by GitLab People Operations to join the Scheme during onboarding:

*  During onboarding, the People Operations Specialist Team informs Vistra Payroll of a new hire that joined the company in the UK.
*  Once Vistra payroll has this information, the employee will be set up on the payroll and added to the pension file for Scottish Widows in preparation to be uploaded.
*  The pension file is sent to Scottish Widows around the end of 6th of each month and will include any joiners/changes/leavers from the previous month’s payroll run. For example everyone who has been included on the pension file in October (the cut off is 20th October) Scottish Widows will be notified around 6th November
*  Any employee who joins after 20th or has not been included on the pension file will be picked up in the following month’s file.
*  Scottish Widows receive the pension file around 6th of each month and will process all joiners/changes/leavers accordingly.
*  New joiners should expect to receive their welcome pack within 14 working days after Scottish Widows receives the pension file.

When an employee joins the scheme they will be sent a welcome pack stating that they are now a scheme member, it will also have their login details. Employees can login to access their own personal fund and can make adjustments as they see fit. Some other key points of the scheme are as follows:  

- The default fund is the Scottish Widows Pension Investment Approach - Balanced.
- There are over 50 funds and 9 lifestyle profile options  
- This is a personal pension which means it can be taken with the employee if they leave GitLab
- Employees will receive tax relief on their contributions, between 20% and 45% depending on their current income tax band
- Each individual plan has an Annual Management Charges (AMC) of 0.70% per annum.
- There are no charges on contributions or transfers into the plan
- There are no charges on transfers out of the plan
- Scottish Widows has a workplace pension webpage with lots of documentation about the scheme. Find the URL in [this document.](https://drive.google.com/file/d/1e38np_Q8_s9Ty5JdbXTiMFXYVPTa20UQ/view?usp=sharing)

### Pension Contribution Percentages

The certification level that has been chosen is **basic salary** only and will not include commission, bonuses or any over time payments. The contributions will be paid monthly and will be shown on the pay-slips.

- From April 6, 2018 these contributions are a minimum of:
    - Employee: 2% of basic salary
    - GitLab:   4% of basic salary
^
- From April 6, 2019 to a minimum of:
   - Employee: 5% of basic salary
   - GitLab:   4% of basic salary
^
- As your contribution is a percentage of your pay, the amounts will automatically increase or decrease in accordance with your basic pay. These contributions will be taken from your pay and will include tax relief.

- You may increase the contribution percentage you pay by contacting People Operations. Please note the company contribution will remain fixed as above. The total maximum contribution you can make is £40,000 p.a.

- These percentages are gross percentages using the [relief at source](https://www.gov.uk/workplace-pensions/managing-your-pension) method. As basic rate tax relief is then added by the pension provider, the amount on your payslip will typically be the net amount, which is 80% of the employee's contribution.

These contribution percentages are in line with [The Pension Regulator's Auto-enrollment legislation](http://www.thepensionsregulator.gov.uk/doc-library/automatic-enrolment-detailed-guidance.aspx).
Should the government's minimum contribution standards change further GitLab will contact you to advise the new levels to comply with legislation.

### Opting Out

If you decide to opt out of the scheme you can do this within one month of the enrollment date by following the instructions in the information from Scottish Widows. The Opt Out deadlines are:

 - You have four weeks from when the Scottish Widows welcome letter is dated.

 - **If you do opt out by this date**, any contributions that have already been taken from your pay will be returned to you and you'll be treated as if you had never joined the pension scheme.

 - **If you don't opt out by this date**, you can stop contributing at any time, in accordance with the pension scheme rules. If you do this, both your contributions and GitLab's, up to that point, will remain invested in your pension pot until you take your benefits.

 - You can also opt-out by completing the Scottish Widows ‘Opt-out form for Group Pension Scheme’.  The form can be found line at http://www.scottishwidows.co.uk/aethirdparty. The form must be printed and completed, then returned to People Operations.

- The pension provider will also be able to tell you when the one month opt-out period started, if you aren’t sure.

 - An opt-out from the pension scheme normally lasts for **three years**. If you've opted out, or stopped contributions to the scheme, GitLab is required to automatically enroll you into the scheme at a later date (normally every three years).

### Contacts

You can contact Scottish Widows or Oakley Financial directly using the contact details in this [pdf](https://drive.google.com/file/d/1e38np_Q8_s9Ty5JdbXTiMFXYVPTa20UQ/view?usp=sharing).

### Re-joining

If you decide to re-join the scheme you can do so by emailing or sending a signed letter to People Operations. If you send an email, it should be from your personal email address, please ensure it contains the phrase `I confirm I personally submitted this notice to join a workplace pension scheme`. You can only re-join once in any 12 month period.

### Reports

Each month payroll will provide reports to People Operations that need to be kept for compliance. These will be filed in the Auto-Enrollment Pension folder on the Google Drive, accessible only to the People Operations team.

### Changing the Pension Scheme Investment Approach

There's a workplace pension webpage linked from this [pdf](https://drive.google.com/file/d/1e38np_Q8_s9Ty5JdbXTiMFXYVPTa20UQ/view?usp=sharing) that contains information about all the options available.

When your policy is first set up, it'll be set to:

- 'Pension Investment Approach' (PIA)
- A lifestyle switching approach for managing your investments as you approach retirement.
- A default investment fund (such as 'Scottish Widows Pension Portfolio Two Pension')

The [policy website](https://personal.secure.scottishwidows.co.uk/) will tell you:

> Although you are currently invested in a Pension Investment Approach you also have other options such as investing in our range of individual funds or changing to a Premier Pension Investment Approach.

Read about all these investment options, and the funds available, on the [workplace pension webpage.](https://drive.google.com/file/d/1e38np_Q8_s9Ty5JdbXTiMFXYVPTa20UQ/view?usp=sharing)

Changing your investment approach or funds could adversely affect your retirement income. Get advice, such as from [Oakley Financial](#contacts).

#### Selecting funds online.

This is an advanced feature that carries significant risk. For team-members comfortable with this, it's not obvious how to enable it.

To access 'investing in our range of individual funds' online you need to call Scottish Widows, and ask:

- To come out of the lifestyle approach.
- To come out of 'Pension Investment Approach' (PIA)
- For online access to the range of funds.

The change may take a couple of days to come into effect, and they'll write and confirm the change.

Having enabled online fund management, you can search for and select the funds on the [policy website.](https://personal.secure.scottishwidows.co.uk/)

The [workplace pension page](https://drive.google.com/file/d/1e38np_Q8_s9Ty5JdbXTiMFXYVPTa20UQ/view?usp=sharing) also has a link to a fund list, plus a PDF which explains:
- The different groups of funds Scottish Widows offers.
- A summary of each fund.

A fund factsheet is provided for each fund with usual information such as previous performance.

Management charges vary by fund and are not on the fact sheets: use the charges sheet tool on the [workplace pension page.](https://drive.google.com/file/d/1e38np_Q8_s9Ty5JdbXTiMFXYVPTa20UQ/view?usp=sharing).

## GitLab LTD United Kingdom Leave Policy

*  Sick Leave
     - Team members are entitled to Statutory Sick Pay (SSP) for up to 28 weeks, to be paid by GitLab. SSP is paid when a team member is sick for at least 4 days in a row (including non-working days); however, team members are not entitled to SSP for the first three "waiting" days. A team member who is out sick for at least four days, but no more than 7 days will need to complete a [Company Self-Certification Form](https://docs.google.com/document/d/1Cy-xUgWwNKroUEEJ0ut80HGVA0d3RJV5mQO-amLjoCs/edit?usp=sharing) and return it to total-rewards@gitlab.com. Team members who will be out sick for more than 7 days must provide a [fit note](https://www.gov.uk/taking-sick-leave) and notify Total Rewards (total-rewards@gitlab.com) as soon as possible.

     - Sick Leave runs concurrently with GitLab PTO. Team members must designate any time off for illness as `Out Sick` in PTO by Roots to ensure that annual sick leave entitlement is properly tracked. 

* Statutory Maternity Leave
     - Mother can take up to a year. Six weeks is paid at the average income over the proceeding 18 weeks (includes commissions, variable pay, etc). The employee is paid at the statutory requirement over the next 33 weeks, and the remaining 13 weeks of the year is unpaid.

* Statutory Paternity Leave:  
     - Paternity - Father is paid for two weeks at the standard rate, but can now share some of the mother’s 52 week period.

The mother may opt to share some of her 52 week maternity leave with her spouse/partner (so long as it is not the first 2 weeks following birth).  The leave period will be paid in accordance with the above regulations for Maternity (ie maximum combined leave of 52 weeks, payments for up to 39 weeks at Statutory Pay rate).
Employees accrue all benefits during leave and must have documentation for risk assessment when notifying the company of pregnancy.
Payroll will continue to be the same while on parental leave, but Vistra will file for reimbursement of any statutory funds once the proper paperwork has been sent.

For a Maternity leave, once an employee notifies GitLab of the pregnancy, People Ops will conduct:
1. [Risk Assessment](https://docs.google.com/document/d/1qHdbaeFSnqdwkQDTEurHD5QMbLPLuZGBQO_CBnRkpiE/edit) – Carry out a risk assessment of a mother's work environment within 1-3 days of notification. This still applies if the employee works from home. The purpose of the assessment is to make the employee aware of any hazards or “risks” to her in the work place/home office to reduce potential injury to her or her baby. This is only required for female employees.
1. For male employees, notify them to fill out the [SC3](https://public-online.hmrc.gov.uk/lc/content/xfaforms/profiles/forms.html?contentRoot=repository:///Applications/PersonalTax_iForms/1.0/SC3&template=SC3.xdp) form and return it to People Ops via email. Once the form is completed, People Ops will forward to Vistra.
1. Notify the employee of the statutory rules/company practice regarding leave, pay, notifications etc with a link to this section of the handbook.
1. At 20 weeks, request MATB1 certificate (ensuring that the original document is kept in BambooHR).
1. One month before the leave begins, the Total Rewards team will loop in GitLab's payroll and Vistra.
  * Compensation will email Vistra a copy of the MATB1, start date of leave, and outline what pay the person is eligible (statutory, supplemental, etc). The payroll will then automatically calculate the SMP due each pay period based on this information. The recovery of 92% will be outlined on the monthly payroll spreadsheet so that the finance team has indication of the amounts being offset each month.
  * As a note, the earliest that the maternity leave can start is 11 weeks before the Expected Week of Childbirth (EWC). Maternity leave will also start automatically the day after the birth of the child (if the baby is early) or if the team member is off work for a pregnancy-related illness in the 4 weeks before the EWC.
  * The team member will be paid through payroll made up of any statutory payments, and any further ‘topped up’ amounts supplemented by GitLab.
  * GitLab will be able to recover from the Government the 39 weeks of statutory maternity pay.
  * Maternity pay is taxable and liable for National Insurance deductions much in the same way that standard pay is.

### Statutory Vacation Leave

Team members are entitled to at least 28 vacation days which consist of 20 days of standard annual leave plus 8 public holidays. These days will accrue from the start date. It is important for UK team members to utilize time off for public holidays as the total amount of annual leave is accrued here inclusive of public holidays. Team members must designate any vacation time taken as `Vacation` in PTO by Roots to ensure that vacation entitlement is properly tracked. Under normal circumstances, there is no carryover for unused vacation days; however, given the situation surrounding COVID-19, the UK government has relaxed measures on the carry over of annual leave for the following two calendar years: 2020-2021 and 2021-2022.

## Employer's Liability Insurance

You can view the `Certificate of Employers Liabilty Insurance` [here](https://drive.google.com/file/d/1eK-_KjE_ITcaryW5QLfP6_v__u3jtaBH/view?usp=sharing).

## Monthly Medical Bill Payments

The Total Rewards Analyst will collect the invoice and provide a department breakdown to Accounts Payable for AXA PPP each month.

1. Send an email to all UK benefits contacts at Vistra on the 10th of the month requesting the AXA PPP invoice for that month. Subsequent emails may be required to ensure timely delivery of the invoice.
1. Once the invoice has been received, add this to the `Monthly AXA Invoices` folder in the `Benefits` Google drive. 
1. Transcribe the PDF into a new tab of the `Monthly AXA Bills` spreadsheet. 
     * The easiest way to do this is to duplicate a previous detail tab, add in the new invoice amounts, and insert any new team members that have been added to the plan. 
1. Run a new Department report for all UK team members from BambooHR and overwrite this in the BambooHR department report tab. 
     * If you duplicated a previous tab when transcribing the PDF, the formula should automatically add everyone's department. Make sure everyone has a department associated with their record in the detail tab before proceeding to the next step.
1. Add a new tab to the spreadsheet for the department summary:
     * Create a pivot table in this tab using the detail tab. 
     * Add "Department" to the "Rows" section of the pivot table builder.
     * Add "Invoice Amount (GBP)" to the "Values" section of the pivot table builder and set this to summarize by "Sum". 
1. Once completed, send an email to Accounts Payable notifying that the invoice and department summary are ready to view.

