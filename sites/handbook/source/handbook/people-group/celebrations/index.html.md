---
layout: handbook-page-toc
title: "Celebrations and Significant Life Events"
description: "Review different ways GitLab celebrates its team members."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Celebrations

### GitLab Anniversary
At GitLab we like to acknowledge and celebrate team member milestones and contributions.  This means that those celebrating their first, third, fifth and tenth hire-date anniversary will receive a token of acknowledgement.

Along with this, all celebrants regardless of length of tenure will receive a mention in the **#team-member-updates** channel which allows the greater GitLab community to share in the celebration!

#### Anniversary Gifts
On the last day of the month of celebration, team members can anticipate an email with a unique link which will lead them to the GitLab Anniversary Gift ordering portal where they can make the relevant size and shipping selections (where applicable).

The average shipping time taking into account current restrictions as a result of global events is three weeks.  In an instance where an order has been placed and not received we encourage team members to reach out to the People Experience Team in the **#anniversary-celebrants** channel.

| Year | Gift | 
| --- | --- | 
| **Year 01** | Celebratory Confetti Tanuki Socks (Unique to the Anniversary Program) |
| **Year 03** | GitLab Tanuki Vest |
| **Year 05** | Travel Bag / Backpack |
| **Year 10** | $500 Travel Grant |

### Team Member Birthdays
GitLab encourages team members to take a day of vacation on their special day in alignment with our **[Paid Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off)**.  If your celebration happens to fall over a weekend please be sure to take an alternate day such as the Friday prior or the Monday after.

Kindly note that team members are welcome to send cards or tokens of acknowledgement to celebrants in their personal capacity however the People Experience Team is unable to do so on behalf of GitLab; a Team or an Individual Team Member.

The GitLab **BirthdayBot** announces the names of team members celebrating their birthday in the **#celebrations** channel.  This is an opt-in announcement and those who wish to be excluded can opt-out by excluding their date of birth from their slack profile.

If you would like to be included in the announcement but would prefer not to have your year of birth noted in your profile you can document your date of birth as follows `0000-MM-DD` using `0000` as the year of birth will ensure only your day and month of birth will be in your profile.

## Significant Life Events
### Flowers and Gift Ordering
The GitLab People Experience Team is able to send Gifts and Flowers on behalf of the Company or a specific Team in acknowledgement of significant life events such as the birth of a little one; well wishes ahead of surgery or the loss of a loved one. Only one order will be sent for a significant life event, if multiple requests are made, the People Experience Team will communicate this further with the manager. 

The event in question must pertain to a GitLab Team Member or the immediate family of a GitLab Team Member and will be allocated to the respective team members departmental budget - the spend range for significant life events is **$75 to $125**. 

We are able to facilitate the ordering of Flowers or Gifts for deliver however are unable to support the purchase of Gift Cards at this time.  In an instance where team members would like to extend the offer of a meal or food delivery service this would need to be expensed by the recipient for reimbursement.

Requests for Gift and Flowers to be ordered can be made using the following [Gift Request Form](https://docs.google.com/forms/d/e/1FAIpQLScxwCUNF-9IV-y-XNswQwkzwA-a6ahuPd8HFGEuxw3EMjukrA/viewform).  Once an order has been placed you will receive a direct Slack message of confirmation from the People Experience Team.

The budget for sending these gifts will come from the department budget of the gift-receiving team member. To make sure the reports in Expensify are routed correctly, People Experience Associates will be sure to fill in the correct division and department. Team members' division and departmental information can be found in their BambooHR profile under the Job tab, in the `Job Information` section.

For procedures on how to handle [gifts](/handbook/people-group/people-experience-team/#gift-requests)

## Support
If you need support around the ordering of gifts and flowers e.g. updating a team members address or editing any of the details submitted please reach our to the People Experience Team via the **#peopleops** channel.

