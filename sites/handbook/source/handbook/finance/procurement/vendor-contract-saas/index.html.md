---
layout: handbook-page-toc
title: Vendor Contract Issue Process
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}

## <i class="fas fa-stream" id="biz-tech-icons"></i> Procurement toolkit
<div class="flex-row" markdown="0" style="height:110px;">
  <a href="/handbook/finance/procurement/vendor-selection-process/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;">Vendor selection process</a>  
  <a href="/handbook/finance/procurement/purchase-request-process/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;">Purchase request process</a>
  <a href="/handbook/finance/procurement/vendor-guidelines/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span style="margin-left: auto; margin-right: auto;">Vendor guidelines</span></a>
</div>

## <i class="fas fa-check-circle" id="biz-tech-icons"></i> How do I request approval for a Software Purchase?

1. Open a Vendor Contract Request issue with [this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request) to begin the process.
1. Create this issue **BEFORE** agreeing to business terms and/or pricing.
1. We will negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term realtionships with vendors. We also continue to evaluate supplier pricing at the time of renewal to minimize our ongoing costs across our long-term relationships with vendors.

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Deep Dive on the Software Vendor Purchase Request Process
Review the [Prior to Contacting Procurement](https://about.gitlab.com/handbook/finance/procurement/prior-to-contacting-procurement/) page first.

   * **NEW VENDOR:** Once you've narrowed your vendor selection process to your final one or two vendors, open the [Software Vendor Contract Request Issue](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request). Let your vendor(s) know they will need to follow the procurement process to finalize the necessary decision making and approval processes. If your contract requires a negotiation, allow for 2-6 weeks to complete all steps in the process which includes internal review, cost analysis, negotiation alignement, negotiation, and contract review and approval. Note this is a rough guideline and not an exact SLA, exact timing depends on the completeness and clarity of business needs. Note the procurement team cannot begin negotiations without all necessary information. If you have specific deadlines please identify those in the due date of the issue and/or in the comment field.

   * **RENEWAL VENDOR:** If you have a renewal coming due with an existing software supplier, open the [Software Vendor Contract Request Issue](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request) 60-90 days in advance of renewal date. Procurement may contact you as well regarding your upcoming renewal. If your vendor asks about the status, let them know the procurement process will need to be followed.

   * **ADD-ON VENDOR:** If you have a need to right-size/purchase additional licenses with an existing vendor, obtain the quote from the vendor and open the [Software Vendor Contract Request Issue](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request).

Complete Steps 1-2, the Intake Steps, with as much specificity and detail as possible. Most of the approvers are hearing about this purchase for the very first time and have no context beyond what you include here. This is your chance to provide all information as clearly as possible to expedite approvals as quickly as possible.

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 1 | Business Needs Intake
This section articulates how the purchase is aligned with the current business needs. Completion of all fields is necessary for approvals. 
Any missing or incomplete fields will result in a delay to the review and/or approval of your request.

**Business Problem Statement**
   * Identify the business problem this purchase will solve. 
   * If this is a renewal, re-state the business problem that the tool currently solves.
   * If this is an add-on, identify the additional problem that the purchase will solve. (i.e. Team members are unable to share single source of truth data and share conflicting data with customers.)

**Business Solution Statement**
   * Identify specifically how this purchase will solve the business problem just identified. Include links to OKR's, PI's, etc.

**Description of Purchase**
   * Itemize exactly what we are buying and what do we get for it? (i.e. SaaS Platform with licenses for 50 people, Sandbox environment for dev and ongoing testing, annual support with SLA's and call center)

**Identify software need (check the one that applies)**
   * New functionality - tech stack link is shared to confirm if your tool is new or not
   * Replaces a tool - Identify which tool this new tool will replace
   * Renewal - Identify the current application
   * Additionall licenses - Identify the application

**Business Justification**
   * Provide 2-3 sentences justifying this spend. (i.e. SaaS tool is a $60K annual investment, due to the efficiencies it will provide we will save $20K in annual labor costs, and $65K in licensing from the old tool we will de-comission.)
   * **If the spend is >$50K,** a more detailed cost analysis is required for approval. Here are some thought starters:
      * Elimination of existing costs (i.e. $65K in terminated SFDC licenses)
      * Headcount savings (i.e. Efficiencies mean we can eliminate planned new hire next quarter for a savings of $25K)
      * Enabled time savings for team (i.e. Currently two team members spend a combined 20 hours a week doing this manually - this tool eliminates that by half. Work focus can shift to delibering Q4 OKR - insert link)
      * Enabled capability/services (i.e. Tool allows us to be compliant with new laws and we can now respond to 5% more RFP's)
      * Essential need (i.e. Note essential is reserved for core business items such as audit, email, laptops. It is not essential to a specific function or a specific OKR or PI, but essential to the company being able to function as a company.)

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 2 | Vendor Needs Intake
This section articulates the information regarding the vendor and the current contract and pricing (if any) that is being proposed. Completion of all fields is necessary for approvals. Any missing or incomplete fields will results in a delay to the review and/or approval of your request.

**Vendor Name, URL, and Specific Application Name**

**Anticipated usage for next 12 months**
   * Confirm if you expect usage to increase or decrease over the next 1-3 years. Tell us when you expect those changes to occur as this can key negotiation levers.

**Vendor contact**
   * The vendor main point of contact for this purchase. More than likely this is the sales rep you are working with. 
   * We will use this name and email to execute the contract signature. If this field is not completed with the correct name and email, your contract signature will be delayed.

**Vendor billing contact**
   * This is NOT the same as the sales rep - even if the rep tells you so :)
   * This is the actual accounts payable contact who our AP team will be coordinating invoice payment with

**Unsigned contract/quotes**
   * This is where we will look to review the proposal. 
   * If you have a renewal or a true up, attach the previous order form here so we can reference terms and pricing. Failure to attach will result in review and approval delays.

**Total Contract Value**
   * Identify the estimated contract value. If it is a multi-year term, identify both the annual and total value. 
      * Note we do not do multi-year agreements except in specific circumstances. 
      * If you believe this is a specific circumstance, identify that here, and add both our CFO and CEO as approvers in Step 4.
   * If this is a new vendor and your spend is greater than $100K, check this box and attach the competitive quote from 1-2 other suppliers.
      * For questions on vendor selection process see [Prior to Contacting Procurement](https://about.gitlab.com/handbook/finance/procurement/prior-to-contacting-procurement/)
   * If this is a new vendor and your spend is greater than $250K, check this box and link the RFP issue here.
      * For questions on vendor selection process see [Prior to Contacting Procurement](https://about.gitlab.com/handbook/finance/procurement/prior-to-contacting-procurement/)

**Integration into our Ecosystem**
   * Identify which existing tools this SaaS offering integrates with. This must be understood to identify any risks to the project, timeline interdependencies, security or privacy concerns.

**Data Categorization**
   * Describe the category of data that will be used including source system, volume, etc. This ensures data needs are understood along with timeline interdependencies and/or security or privacy concerns.

**Personal Data Privacy Review**
   * If any personal data will be collected, hosted, shared, or processed in any way, a [Privacy Review Issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=Vendor-Procurement-privacy-review) MUST be completed. **Failure to complete this issue can result in a significant delay to the approval of your purchase.**
   * If this is a renewal or add-on, link to the previous Privacy Review issue to expedite review and approval. Note it was formerly referred to as a DPIA.

**Contractor Involvement?**
   * State if there will be any professional services/contractors engaged as part of this purchase. Sometimes the software company will provide implementation support who would need access to our systems. If so, idenitfy if they will have access to non-public data. This is necessary for security and privacy review.

**Data Reporting**
   * If there is a need to report on data from the application we are purchasing, submit a [data team discovery issue](https://gitlab.com/gitlab-data/analytics/-/issues/new?issuable_template=Extraction%20Discovery). This ensure any interdepenencies or resources from other teams needed are identified before creating roadblocks.

**Related Documentatioin**
   * Add links to any requirements, sales proposals, etc. that are relevant to this purchase.

**Marketing Campaign Finance Tag**
   * If, and only if, this is a marketing request related to a campaign, please add the tag directly from the finance budget doc here to avoid delays in budgeting and payments.

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 3 | Security Review

Review our [Third Party Risk Management Procedure](/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html) and submit a request accordingly. 

To expedite the risk assessment process, complete the identified check boxes as relative:

**Provide Vendor Security Contact Name and Email Address**
   * This allows our security team to contact their security team directly to address any questions as efficiently as possibly. Please request this information from your sales contact.

**Security Documentation**
   * Request your vendor contact share with us independent certifications validating the security protocols in place for our review.
   * Send this email template [requesting security documentation](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html#email-request-template) to your vendor contact

**Non Disclosure Agreement (NDA)**
   * If this is a **NEW vendor**, an NDA is oftentimes required from the vendor in order to share security certification
   * Most often, an NDA is not needed from an existing vendor to share security certifications since confidentiality terms are a part of the existing agreements in place. If, however, your vendor has a specific concern and would like an NDA in place, we can accomodate.
   * Send our [Mutual NDA template](https://docs.google.com/document/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/edit) to your contact, requesting they sign and return back to you in order to expedite the security review process.

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 4 | Approvals
Tag your functional leader according to the [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix) in the first section of the table.

Tag your finance business partner under Budget Approval.

Scroll to the bottom of the issue and select a due date, if relative, and we will do our best to accomodate. All reviewers and approvers are notified so long as you do not edit below the section that says DO NOT EDIT BELOW.

**SUBMIT YOUR ISSUE!**

Now the review and approval process can begin. 

**A contract can not be signed until each function has reviewed and approved**

Here is a deep dive summary on what each function is reviewing for approval:

**Functional Review and Approval**
1. Please consult the [Authorization Matrix](/handbook/finance/authorization-matrix/) to determine who must sign off on Functional Approval and Financial Approval.  
1. The approval from the functional leader is confirming they approve this specific spend to this specific vendor, and that it aligns with their specific business goals as a functional leader.

**Technology Review and Approval**
1. The purchase request is reviewed by our VP, IT
1. The request is reviewed to ensure the software aligns with overall technology architecture and to identify any timeline and/or implemtnation bottlenecks

**Budget Review and Approval**
1. Finance is responsible for confirming the purchase is in budget.
1. Finance is responsible for reviewing and validating the Business Justification. If questions or concerns on the Business Justification, fp&a partner will engage VP, Finance.
1. If the contract exceeds $100K, the CFO will be added as an additional approver.
1. Finance validates the Department, and GL Account.
1. Finance validates the correct entity:
   * PEO's, Contractors should be engaged with GitLab IT BV
   * Contract to be engaged locally when there is a GitLab entity available (e.g. Netherlands with BV, UK with Ltd et cetera)
   * If there is no GitLab entity available in the country of a vendor use GitLab Inc

**Security Review and Approval**
1. Security is responsible for reviewing vendor security practicies and will adhere to the Third Party Vendor Security Review Process. 
1. The Security Compliance team needs 3 business days to complete this review from the time they receive all necessary documentation from the vendor 
1. Once the security team approves the vendor and/or identifies gaps in the vendor's security practices for negotiation, security will provide their approval in the issue.
1. Consult the [Data Classification Policy](https://docs.google.com/document/d/15eNKGA3zyZazsJMldqTBFbYMnVUSQSpU14lo22JMZQY/edit#heading=h.a7l25bv5e2pi) to understand whether your contract will need security review. Any contracts that will share RED or ORANGE data will need security approval prior to signing. Additionally, an annual reassessment of vendor's security posture is performed as part of the contract renewal.
1. Complete a [Privacy Review](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=Vendor-Procurement-Privacy-Review); please note that this will be done in partnership with GitLab's Privacy Officer and reviewed by Security Compliance during the Security Review.

**Procurement Review and Approval**
1. Before approving issues, the procurement team verifies:
   * Correct purchase request issue type was used.
   * Correct individuals were tagged and have approved according to the [Authorization Matrix](/handbook/finance/authorization-matrix/)?
   * Vendor Selection and Negotiation process was followed
   * Contract is Reviewed
      * Procurment confirms dollar amounts match, confirm currency, confirm entities match in quote, and general scope is addressed.
      * If entities do not match, procurement will tag the issue creator and budget approver to confirm which entity should be used. 
      * If negotiated, procurement owns all commercial terms in the contract review process.
2. Procurement Approval Authority
   * Procurement Ops Analyst can approve purchase requests up to $25K 
   * Purchase requests >$25K will be approved by the Sr. Mgr Procurement
3. Approval Process
   * In the event procurement approves the purchase request issue before the other approvers to avoid being a bottleneck, procurement will approve before all parties.
   * If this happens, procurement will note our approval as pending the remaining approvers. 
   * At this point it is the responsibility of the issue owner as the DRI to follow the remaining process and secure remaining approvals BEFORE obtaining contract signature.

**Legal Review and Approval**
1. Legal is responsible for reviewing vendor contracts and will adhere to the legal playbook.
1. A contract cannot be signed until it has been **stamped** with approval by the legal team. Once the legal team approves the contract, legal will upload the contract with the approval stamp. 

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 5 | Contract Signature
1. Do not send the contract to the authorized signatory until all approvals are received. Doing so can put GitLab in a direct financial and/or legal risk which could need to be escalated.
1. Once all approvals in the issue are received, send the contract to the authorized signatory:
     - Upload the contract with the legal stamp to HelloSign
     - If a legal stamp is not included in the issue, please request clarification from legal and/or procurement by tagging them in the contract issue or asking for clarification in the #procurement slack channel.
     - In the description field in HelloSign, paste the link to the vendor contract issue request to avoid delays in signature.
     - Enter the signatory's name and email in HelloSign
 1. Once the contract is signed by GitLab, send the contract to your vendor (if not already signed by the vendor) through HelloSign.
 1. After the contract is signed by both parties, upload the fully executed contract to ContractWorks. You will need to upload the fully signed pdf into the folder labeled **01. To Be Standardized**, which in under the parent folder **01. Uncategorized**. Legal will then organize the contracts using their [instructions and best practices](/handbook/legal/vendor-contract-filing-process)
      - If you need access to ContractWorks, please process an access request [here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single+Person+Access+Request).

- Note: If your vendor has a signature tool they would like to use and all approvals have been received in the contract issue, request that the vendor send the final contract version with the legal stamp for signature and also include the issue link. This will avoid delays in signature.

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 6 | Vendor Payment and Final Contract
Similar to our Access Request process for team members, we have an access request process for consultants or professional services providers. If the vendor requires access to systems to complete work, the vendor manager (ie. the GitLab team member who will manage the relationship with the temporary service provider, generally a people manager) is responsible for creation of a Vendor Access Request and Orientation issue. 

These issues aren't created in the same location as access requests for employees so find the link below so use this [access request template](https://gitlab.com/gitlab-com/contingent-workers/lifecycle/-/issues/new?issuable_template=access-request) and assign it to yourself and the relevant provisioner(s) for the tools that the professional services provider requires access to. In the AR, include [**only systems that are necessary**](/handbook/engineering/security/access-management-policy.html#access-management) to the work that the vendor will be performing. 

Create an [orientation issue](https://gitlab.com/gitlab-com/contingent-workers/lifecycle/-/issues/new?issuable_template=orientation-issue) if the professional services provider wants support through the set up of the most common tools used at GitLab. Assign to yourself and the professional services provider if they have a GitLab account with the required access.

## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 7 | Accounts Payable
1. Vendors will be required to create an account within Tipalti in order to receive payment
1. For complete details on how to obtain payment, please visit Accounting's [Procure to Pay](/handbook/finance/accounting/#procure-to-pay) page.
1. If your annual contract value is equal to or greater than $100K, a Purchase Order must be created to pay the vendor. See Creating a Purchase Order for steps to do so.

If you have additional questions, please ask in #procurement slack channel. Or attend Purchasing Office Hours, available in the GitLab Team Calendar.
