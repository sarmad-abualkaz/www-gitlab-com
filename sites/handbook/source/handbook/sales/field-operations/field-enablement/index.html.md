---
layout: handbook-page-toc
title: "Field Enablement"
description: "The Field Enablement team's mission is to help customers successfully grow and evolve in their journey with GitLab to achieve positive business outcomes with effective enablement solutions aligned to Gitlab’s values"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The Field Enablement team's mission is to help customers successfully grow and evolve in their journey with GitLab to achieve positive business outcomes with effective enablement solutions aligned to [Gitlab’s values](/handbook/values/). How we do this:
- Collaborate with cross-functional stakeholders (including but not limited to Sales, Customer Success, Marketing, Sales Ops, Product/Engineering, and the People team) to address prioritized knowledge, behavior, and skill gaps in the GitLab Field Team to achieve desired business outcomes.
- Define, coordinate, and/or lead the development and delivery of effective enablement solutions (training, technology, knowledge, content, process, and tools) for GitLab Sales, Customer Success, and Partners throughout all phases of the customer journey as well as different stages of each team member's tenure and/or experience at or with GitLab.
- Champion the needs and interests of the Field Team (all routes to market) – ensuring that they are regularly informed about relevant business and organizational updates and have access to enablement materials necessary for their day-to-day work to meet and exceed business objectives.
- Cultivate a culture of curiosity, iteration, and continuous learning & development across the GitLab field organization.

## Contact Us 
- Want to chat with us? Please Slack us at **[#field-enablement-team](https://gitlab.slack.com/archives/field-enablement-team)**. 
- Have a Field Enablement request? Please use our **[quick request process](/handbook/sales/field-operations/field-enablement/#making-a-field-enablement-request)** below.

## Key Programs

-  [Sales Onboarding](/handbook/sales/onboarding/)
-  [Command of the Message](/handbook/sales/command-of-the-message)
-  Continuous Learning
     - [Customer Success Skills Exchange](/handbook/sales/training/customer-success-skills-exchange)
     - [Sales Enablement Level Up Webcast Series](/handbook/sales/training/sales-enablement-sessions/)
     - [Sales Training Resources](/handbook/sales/training/)
     - [Technical Questions for Sales](/handbook/sales/training/technical-questions-for-sales/)
-  [Channel Partner Training, Certifications and Enablement](/handbook/resellers/training/)
-  [Field Certification Program](/handbook/sales/training/field-certification)
     - [Sales Operating Procedures](/handbook/sales/sales-operating-procedures/)
     - [Field Functional Competencies](/handbook/sales/training/field-functional-competencies/)     
- [Field Manager Development Program](/handbook/sales/field-manager-development/)
     - [Sales Manager Best Practices](/handbook/sales/field-operations/field-enablement/sales-manager-best-practices)
- [Field Communications](/handbook/sales/field-communications/)
     - [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/)
     - [Monthly GitLab Release Email to Sales](/handbook/sales/field-communications/monthly-release-sales-email)
- Sales Events
     - [Sales Kickoff](/handbook/sales/training/SKO)
     - [Sales QBRs](/handbook/sales/sales-meetings)
     - [GitLab President's Club](/handbook/sales/club/)

## Strategy

<!-- blank line -->

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQGUQ9g98p31bPZ267en01qJCqgjeX8ZC6GmChBTKz7TV0OEwhFlKbXPgf1YARh5V-lDBegFpu60iTL/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

<!-- blank line -->

## Handbook-First Approach to GitLab Learning and Development Materials

Chat between David Somers (Sr. Director, Field Enablement) and Sid Sijbrandij (CEO)
<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->

### Key Discussion Points
- Our [Mission](/company/strategy/#mission) is that Everyone Can Contribute, and our most important value is [Results](/handbook/values/#results). Like we've extended that to the Handbook, we want to extend it to our Learning Materials.
- We want to leverage the best of an e-learning platform, with the benefits of reminders, interactivity, and more but make sure the materials we produce are also available to those who aren't using an e-learning platform, while fulfilling [our mission](/company/strategy/#mission).
- There are benefits to keeping our e-learning material [handbook-first](/handbook/handbook-usage/#why-handbook-first):
    - Folks who have already completed a formal training through an e-learning platform may want to return to the materials
    - Those who never go through the formal platform may also benefit from the materials
    - The handbook continues to be the SSOT, with the e-learning platform leveraging handbook materials through screenshots, embeds, and more

Learn how Field Enablement takes a [Handbook-first approach to interactive learning](/handbook/people-group/learning-and-development/interactive-learning/).

## Six Critical Questions

Inspired by _The Advantage: Why Organizational Health Trumps Everything Else in Business_ by Patrick Lencioni

<details>
<summary markdown="span">1. Why does the GitLab Field Enablement team exist?</summary>
See our team mission in the [Overview](/handbook/sales/field-operations/field-enablement/#overview) section.
</details>

<details>
<summary markdown="span">2. How do we behave?</summary>

On our best day, we show up with a positive attitude while demonstrating [GitLab’s values](/handbook/values/) along with the following behaviors to overcome the [Five Dysfunctions](/handbook/values/#five-dysfunctions):

- **Trust**: Extend trust, actively listen, and assume noble intent; give and receive feedback with respect and solicit feedback often
- **Embrace Healthy Conflict**: Engage in constructive conflict for the purpose of achieving shared goals & objectives; resolve personal issues, quickly and directly
- **Commitment**: Support decisions once decisions are made with a GitLab team-first approach
- **Accountability**: Hold ourselves and each other accountable while encouraging each other & celebrating successes
- **Results**: Strong drive for results and a focus on the customer; demonstrate passion for continuous learning & improvement

</details>

<details>
<summary markdown="span">3. What does the Field Enablement team do?</summary>
See "How we do this" in the [Overview](/handbook/sales/field-operations/field-enablement/#overview) section.
</details>

<details>
<summary markdown="span">4. What does success look like?</summary>

The below is a work in progress as we define success measures across each stage of the customer journey:

- **Engage & Educate the Customer**
    - Increase # of rep-sourced opps
    - Accelerate sales cycle time and improve conversion of MQLs to SAOs
    - Accelerate and improve predictability of new rep ramp time
- **Progress the Opportunity & Close the Deal**
    - Increase # of closed deals per rep
    - Accelerate sales cycle time and improve conversion of SAOs to Closed/Won deals
    - Increase average sale price (inclusive of improved product mix to sell more Premium and Ultimate)
    - Accelerate and improve predictability of new rep ramp time
    - Improve forecasting accuracy
    - Improve win rates
- **Retain & Expand**
    - Improve renewal rates (inclusive of up-sell and cross-sell)
    - Accelerate customer time to value
    - Increase breadth of stage adoption

</details>

<details>
<summary markdown="span">5. What is most important right now (1HFY22)?</summary>

[**Q1FY22 Field Enablement OKRs**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/660)

1. **Improve field efficiency (all routes to market) with effective training solutions to address prioritized knowledge and skill gaps**
    - Collaborate wwith Alliances team to deliver internal enablement for 3 hypercloud partners (AWS, GCP, IBM) ([issue](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/1953))
    - Field certification program (Sales, SA, TAM, and Partner Sales & SA audiences)
    - Design & begin to deliver a Manager Training program for the field org ([epic](https://gitlab.com/groups/gitlab-com/sales-team/-/epics/46))
    - Continue to operationalize and reinforce Command of the Message & MEDDPPICC
1. **Promote a culture of continuous learning that activates intrinsic motivation for improvement & development**
    - Improve learning experience by continued roll-out of new Learning Experience Platform (LXP)
    - Develop and execute robust change management plan that taps into team members’ inherent desire to pursue excellence
    - Continued execution & ongoing iteration of continuous learning programs (CS Skills Exchange and Sales Enablement Level Up Webcast series)
1. **Ensure that the GitLab field organization is regularly informed about relevant business and organizational updates and has access to information & resources necessary for their day-to-day work**
    - Deliver an effective Sales Kickoff 2021
    - Continuous execution & iteration of field communications strategy
    - Make it easier for Sales to discover & access information/resources they need when they need it

</details>

<details>
<summary markdown="span">6. Who must do what?</summary>

- **[Sr. Director, Field Enablement](/job-families/sales/director-of-field-enablement/#senior-director-field-enablement)**
    - [David Somers](/company/team/#dcsomers)
- **[Manager, Field Enablement Programs](/job-families/sales/director-of-field-enablement/#manager-field-enablement-programs)**
    - [Kris Reynolds](/company/team/#kreynolds1)
- **[Program Managers: Enterprise Sales, Commercial Sales, and Technical Sales / Customer Success](/job-families/sales/program-manager-field-enablement/)**
    - [Kris Reynolds](/company/team/#kreynolds1) (Customer Success Enablement)
    - [Kelley Shirazi](/company/team/#kelley-shirazi) (Commercial Sales Enablement)
    - [Alvaro Warden](/company/team/#awarden) (Partner Enablement)
    - TBH (Enterprise Sales Enablement)
- **[Field Onboarding Program Manager](/job-families/sales/program-manager-field-enablement/)**
    - [Tanuja Paruchuri](/company/team/#tparuchuri)
- **[Sales Training Facilitator](/job-families/sales/sales-training-facilitator-field-enablement/)**
    - [John Blevins](/company/team/#jblevins608)
- **[Field Communications Manager](/job-families/sales/field-communications/)**
    - [Monica Jacob](/company/team/#monicaj)
- **[Technical Instructional Designer](/job-families/sales/technical-instructional-designer/)**
    - [Issac Abbasi](/company/team/#iabbasi)

</details>

## Making a Field Enablement Request

If you have a great training idea and/or enablement request, we'd love to hear it! Please make an enablement request by following this quick process:

- Submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/new?issuable_template=field-enablement-request)
    - Please be descriptive in the issue in order to give our team the necessary context to review and prioritize your request. Note that ALL questions must be answered prior to Field Enablement triaging your request.
- Upon submitting your request, the issue will be tagged as `FE priority::new request` and `FE status::triage`
- Field Enablement will triage the request as quickly as possible. Upon examining the request, they will assess the priority and status by labeling it with one of the labels (please see label definitions in section below):
    - Priority:
        - `FE priority::1`
        - `FE priority::2`
        - `FE priority::3`
    - Status:
        - `FE status::wip`
        - `FE status::backlog`
- Field Enablement will collaborate with the requester on the request details directly in the issue to clarify any missing information, set up a discovery call, and/or update on next-steps.

If you have a Field Communications request (process change, announcement, etc.), please see the request process outlined [here](/handbook/sales/field-communications/#requesting-field-announcements).

## Field Enablement groups, projects, and labels

- **Groups**
    - Use the GitLab.com group for epics that may include issues within and outside the Sales Team group
    - Use the GitLab Sales Team group for epics that may include issues within and outside the Field Operations group
- **Projects**
    - Create issues under the “Enablement” project
- **Labels**
    - **Team labels**
        - `field enablement` - issue initially created, used in templates, the starting point for any label that involved Field Enablement
        - `FieldOps` - label for issues that we want to expose to the VP of Field Operations; these will often mirror issues with the `FE priority::1` label
    - **Stakeholder/Customer labels**
        - `FE:CS enablement` - label for Field Enablement issues related to enabling Customer Success (CS) roles
        - `FE:sales enablement` - label for Field Enablement issues related to enabling Sales roles
        - `FE:partner enablement` - label for Field Enablement issues related to enabling Partners
    - **Initiative labels**
        - `field certification` - label for issues related to /handbook/sales/training/field-certification/
        - `field communications` - label for items that include work by/with the Field Communications team within Field Enablement
        - `field events` - label for Field Enablement-supported events (e.g. QBRs, SKO, President's Club, etc.)
        - `force management` - label for issues related to Force Management engagement
            - `vff` - label for Value Framework Feedback
            - `vff::new` - starting point for Value Framework feedback
            - `vff::accepted` - Value Framework feedback that will be actioned on
            - `vff::deferred` - Value Framework feedback that will be deferred until more information is gathered
            - `vff::declined` - Value Framework feedback that is declined (no action will be taken)
        - `lxp` - label for GitLab Learning Experience Platform    
        - `sales onboarding` - label for issues related to sales/field onboarding
        - `QBR` - requests from Sales QBRs
        - `sales enablement sessions` - label for weekly virtual sales enablement series
        - `sko` - label for issues related to Sales Kickoff
        - `status:plan` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized but not yet scheduled
        - `status:scheduled` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized and scheduled
        - `strategy` - plans, methods, or series of maneuvers or stratagems for obtaining specific goals or results
    - **Status labels**
        - `FE status::triage` - assigned to new requests before priority status is determined
        - `FE status::wip` - work in progress that has been accepted and assigned to a DRI
        - `FE status::backlog` - things in the queue not currently being worked
    - **Priority labels**
        - `FE priority::new request` - label for new requests that have not yet been prioritized
        - `FE priority::1` - work that directly supports an OKR
        - `FE priority::2` - work that does not directly support an OKR but has a large impact on the field team
        - `FE priority::3` - work that does not directly support an OKR and has a low to medium impact on the field
- **Field Enablement Issue Boards**
    - [Field Enablement By Initiative Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1191445)
    - [Field Enablement By Priority Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1644552?&label_name%5B%5D=field%20enablement)
    - [Sales Enablement Sessions Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1231617)
    - [Customer Success Skills Exchange Board](https://gitlab.com/gitlab-com/sales-team/cs-skills-exchange/-/boards/1414538)
    - [Sales & CS Onboarding Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1790236)
    - [Partner Enablement Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1645038)
    - [Field Certification Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1637426)
    - [Command of the Message and MEDDPPICC Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1823684)
    - [Commercial Sales Enablement Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1790139)
