description: A bleeding-edge role, DevOps platform engineers fill the space
  between hardware and software.
canonical_path: /topics/devops/what-is-a-devops-platform-engineer/
parent_topic: devops
file_name: what-is-a-devops-platform-engineer
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: What is a DevOps platform engineer?
header_body: A bleeding-edge role, DevOps platform engineers fill the space
  between hardware and software.
body: >-
  ## What is a DevOps platform
  engineer?


  In the complex world that can be DevOps software development, a platform that can be deployed as a single application promises to bring all the disparate forces together. It’s tough to argue against [the obvious benefits of a single solution](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html), but someone has to “own” the platform.


  Enter the DevOps platform engineer, a bleeding-edge role that is just now popping up on job search sites like [Stack Overflow](https://stackoverflow.com/jobs?q=DevOps+platform+engineerl). Although some would argue a platform engineer is just another name for a [site reliability engineer](https://opensource.com/article/18/10/sre-startup), the advent of complex new technologies including Kubernetes, microservices, containers, and cloud orchestration have pushed some companies to create a platform engineering team (or teams) charged with overseeing the platforms and the related technologies.


  ## A new DevOps team member


  A quick look at advertised job responsibilities and qualifications shows how a DevOps platform engineer might operate in a DevOps team. In general, a platform engineer’s role is to help developers get software out the door more quickly and with security in mind. As such, it’s not surprising companies are looking for DevOps platform engineers with:


  > CI/CD and other automation experience

  >

  > Familiarity with Kubernetes, infrastructure as code, containers, and container orchestration technologies

  >

  > Extensive experience with cloud deployments and a DevOps team

  >

  > Knowledge of secure coding practices including OWASP, secrets management, and vulnerability remediation.

  >

  > Strong programming chops and deep familiarity with Linux/Unix


  It’s important to remember, however, that the responsibilities of a DevOps platform engineer could vary widely depending on the type of organization. A greenfield company with no legacy systems is likely to have cloud expertise baked in, while an enterprise (and its presumptive legacy systems) may need extra help when it comes to migrations.


  ## How it works in a DevOps environment


  There’s no question that a DevOps platform engineer plays a pivotal role sitting between Dev and Ops, but leaning more toward operations. One company wrote about their [DevOps platform engineering journey](https://medium.com/seek-blog/platform-engineering-why-we-dont-need-a-devops-team-e88c8b97cc4f) and said at the end of the day their focus was on operations and site reliability. Others have suggested a DevOps platform engineer must be responsible for seamless “self-serve” production for developers, as well as monitoring, alerting, and even potentially evangelism for the platform itself.


  It is likely this role will continue to evolve as time passes and the DevOps platform gains more traction.
resources_title: Read more about DevOps platform
resources:
  - title: Give devs the power to deploy and double your efficiency
    type: Case studies
    url: https://about.gitlab.com/customers/glympse/
  - url: https://www.youtube.com/embed/MNxkyLrA5Aw
    type: Video
    title: The benefits of a single application explained in a video (2.5 minutes)
  - title: How the idea of a DevOps platform was born in Poland
    url: https://about.gitlab.com/blog/2020/10/29/gitlab-hero-devops-platform/
    type: Blog
