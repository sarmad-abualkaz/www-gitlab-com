title: DevOps platform
description: A DevOps platform brings tools together in a single application so
  everyone involved in the software development process -- from a product
  manager to an ops pro -- can seamlessly work together to release software
  faster.
header_body: A DevOps platform brings tools together in a single application so
  everyone involved in the software development process -- from a product
  manager to an ops pro -- can seamlessly work together to release software
  faster.
canonical_path: /topics/devops-platform/
file_name: devops-platform
twitter_image: /images/opengraph/gitlab-blog-cover.png
related_content:
  - title: Test your open DevOps platform knowledge
    url: https://about.gitlab.com/quiz/devops-platform/
  - title: "Why you need a DevOps platform team "
    url: /topics/devops/how-and-why-to-create-DevOps-platform-team.index.html
  - title: Reduce the cost of a DevOps platform
    url: /topics/devops/reduce-devops-costs/
  - title: What is a DevOps platform engineer?
    url: /topics/devops/what-is-a-devops-platform-engineer/
  - title: Get the most out of your DevOps platform
    url: /topics/devops/seven-tips-to-get-the-most-out-of-your-devops-platform.index.html
  - title: Choose a DevOps platform to avoid the DevOps tax
    url: /topics/devops/use-devops-platform-to-avoid-devops-tax.html
  - title: 10 key features to look for in a DevOps platform
    url: /topics/devops-platform/ten-key-devops-platform-features/
body: >-
  ## What is a DevOps platform?


  A DevOps platform is a unified application where every part of the DevOps process — from idea creation to release — is tied together in a single tool. A DevOps platform brings everyone involved in software creation – and every tool necessary – together at the same time, saving time and resources and ensuring faster software delivery.


  On the surface, [DevOps](/topics/devops/) brings devs and ops together; the reality, however, is quite a bit more complex as security, testers, product managers, UX designers, finance, the legal team, and even marketing all have a hand to play when it comes to creating and releasing software. A DevOps platform gives all of the players involved a single place to communicate, collaborate, gather data, and analyze results – there’s no more hunting around for information or being left out of the loop.


  A DevOps platform also eliminates all the issues surrounding toolchain sprawl, maintenance, and integration. A platform provides a single source of truth which streamlines the technical development process dramatically.
benefits_title: Benefits of a DevOps platform
benefits_description: ""
benefits:
  - description: From code reviews to automated testing, incident management and
      monitoring, using a unified platform means every single part of DevOps is
      much more straightforward and, in some cases, actually doable for the
      first time.
    title: Ease of use
    image: /images/icons/computer.svg
  - title: Better collaboration
    description: Business partners can actually see what’s going on with software
      development, release cycles, and customer feedback all in one place.
      Software teams will have [fewer
      miscommunications](/blog/2020/11/23/collaboration-communication-best-practices/),
      too.
    image: /images/icons/collaboration-icon.svg
  - description: More testing, baked earlier into the process, means improved
      security, faster releases, and improved customer satisfaction.
    title: Safer code
    image: /images/icons/shield-checkmark.svg
  - title: Tighter feedback loops
    description: Visibility and [traceability](/blog/2020/01/30/insights/) are the
      hallmarks of a DevOps platform because everything is in one place.
      Troubleshooting has never been easier.
    image: /images/icons/agile.svg
  - title: Performance monitoring
    description: A DevOps platform allows teams to stop guessing at how software
      will work and actually see real world results.
    image: /images/icons/trending-icon2.svg
  - title: Fewer compliance headaches
    description: Move to a DevOps platform and suddenly all of those things that had
      to be tracked and recorded will be handled automatically.
    image: /images/icons/computer-test.svg
  - title: Less technical debt
    description: It’s easy to get even non-developers on board with reducing
      technical debt when everyone can see the burden it imposes, thanks to a
      single DevOps platform.
    image: /images/icons/piggy-bank.svg
  - title: Save time, save money
    description: "A DevOps platform saves teams time (fewer tools to integrate,
      update, and maintain) and money (fewer tools to purchase, period). "
    image: /images/icons/gitops-benefits-costs.png
benefits_2_title: Get ready for a DevOps platform
benefits_2_description: >-
  Want to make sure your team is ready to get the most out of a DevOps platform?
  Here are seven things to consider before you begin:


  1. Do you *really* understand your team’s _workflow_? You won’t get the biggest benefit from a DevOps platform if you don’t set it up to reflect the reality of how your team operates. There’s no right or wrong here.

  1. _Culture_ matters and that’s particularly true when it comes to rolling out a DevOps platform. Make sure your messaging about a new DevOps platform will resonate with your team and organizational culture.

  1. It’s all about _deployments_, so it makes sense to understand exactly how your team [deploys](/blog/2020/07/23/safe-deploys/) now, what the hiccups are, and how you plan to address them before you add a DevOps platform to the mix.

  1. _Security_ is top of mind for many teams and a DevOps platform can make that even easier. Make sure to assess where your team is today, and what the goals are, and set simple goals to achieve. A DevOps platform can streamline security but the best way to proceed is through what we call [iteration](/blog/2020/02/04/power-of-iteration/) or small changes.

  1. A DevOps platform is a great opportunity to add in some _advanced technology_ like [machine learning](/blog/2020/12/01/continuous-machine-learning-development-with-gitlab-ci/) or artificial intelligence. Take the time before rolling it out to consider what your team might like to experiment with.

  1. A _“minimum viable product”_ is a worthy goal (well, we think so at GitLab) and it means the product is ready for prime time, but just ready. Considering an “MVP” mindset makes sense as you also consider a DevOps platform – the platform will help teams get to an MVP sooner and the other bonus of an MVP is teams can continue to iterate on it until it’s completely ideal.

  1. Finally, a unified DevOps platform allows unique _visibility and traceability_ through the entire software development lifecycle. Make sure your team and all other software development stakeholders take advantage of these features.
resources_title: Resources
resources_intro: Here’s a list of resources on DevOps platforms. This is a new
  and growing area, so please share your favorites with us via Twitter
  [@gitlab](https://twitter.com/gitlab).
resources:
  - title: How a DevOps platform makes everything simple
    url: https://www.youtube.com/watch?v=TUwvgz-wsF4
    type: Video
  - title: Why its important to integrate monitoring and deployment
    url: https://www.youtube.com/watch?v=ihdxpO5rgSc
    type: Video
  - title: Why continuous testing matters
    url: https://www.youtube.com/watch?v=tQy0O1EGixs
    type: Video
  - title: How to simplify DevOps
    url: https://www.youtube.com/watch?v=TUwvgz-wsF4
    type: Video
  - title: Goldman Sachs streamlines its DevOps efforts
    url: /customers/goldman-sachs/
    type: Case studies
  - title: BI Worldwide discovers the benefits of a single tool
    url: /customers/bi_worldwide/
    type: Case studies
  - title: The European Space Agency and DevOps
    url: /customers/european-space-agency/
    type: Case studies
  - title: GitLab’s 2020 Global DevSecOps Survey
    url: /developer-survey/
    type: Reports
  - title: Gartner on application release orchestration
    url: /blog/2020/01/16/2019-gartner-aro-mq/
    type: Blog
  - url: https://about.gitlab.com/customers/axway-devops/
    type: Case studies
    title: Axway leverages the benefits of a DevOps platform
schema_faq: []
